#include "main.h"
#include "state.h"
#include "led.h"

#include "uart.h"

#ifdef __cplusplus
extern "C"
#endif


int main(void)
{
	
	STATE_vInit();
	
	while (1) 
	{
		
		STATE_vMain();
		
	}

}
