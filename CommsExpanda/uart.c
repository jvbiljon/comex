#include "uart.h"


UARTEVSE_tstRxFifo UARTEVSE_RxFifo;
UARTEVSE_RxPacket_t UARTEVSE_RxPacket;

UARTPayment1_tstRxFifo UARTPayment1_RxFifo;
UARTPayment1_TxPacket_t UARTPayment1_TxPacket;

UARTPayment2_tstRxFifo UARTPayment2_RxFifo;
UARTPayment2_TxPacket_t UARTPayment2_TxPacket;

UART_State_t UARTEVSE_State = WaitResponse;
UART_State_t UARTPayment1_State = WaitResponse;
UART_State_t UARTPayment2_State = WaitResponse;

UART_Mode_t UARTEVSE_Mode = RecieveData;

uint8_t emptyBufferMessage[8] = { UART_START_BYTE_TX, '0', '0', UART_REQ_NOMOREPACKETS, '0', UART_STOP_BYTE_TX_1, UART_STOP_BYTE_TX_2 };

static struct
{
	UART_EVSETxPacket_Buffer_t UART_EVSETxPacket_Buffer[UART_EVSE_NUMOFPACKETSTXBUFFER];
	uint8_t UART_EVSETxPacket_Store;
	uint8_t UART_EVSETxPacket_Sent;
} UART_EVSETxBuffer_Term1;

static struct
{
	UART_EVSETxPacket_Buffer_t UART_EVSETxPacket_Buffer[UART_EVSE_NUMOFPACKETSTXBUFFER];
	uint8_t UART_EVSETxPacket_Store;
	uint8_t UART_EVSETxPacket_Sent;
} UART_EVSETxBuffer_Term2;


// TEMP: TO REMOVE??
/*
#define USART1TXBUFFERSIZE	50
#define USART1TEMPBUFFSIZE	2
uint8_t USART1TXBUFFER[USART1TXBUFFERSIZE] = "0";
uint8_t USART1TEMPBUFF[USART1TEMPBUFFSIZE];


void USART1_vClearTxBuffer(void)
{
	uint16_t i = 0;
	for (i = 0; i < USART1TXBUFFERSIZE; i++)
	{
		USART1TXBUFFER[i] = 0;	
	}
}

void USART1_vClearRxBuffer(void)
{
	uint16_t i = 0;
	for (i = 0; i < USARTEVSE_RXBUFFERSIZE; i++)
	{
		UARTEVSE_RxFifo.USARTEVSE_RXBUFFER[i] = 0;	
	}
}

void USART1_vClearRxIndex(void)
{
	UARTEVSE_RxFifo.u8Rx1WriteIndex = 0;	
}

void USART1_vSend(char data[], uint16_t u16size)
{
	uint16_t i = 0;
	
	// Clear Buffer for Writing 
	USART1_vClearTxBuffer();
	// Convert string buffer to correct format 
	while (data[i] != 0)
	{
		USART1TXBUFFER[i] = (uint8_t) data[i];	
		i++;
	}

	// Now send the String 
	USART1_vTransmitString(u16size);
}

void USART1_vTransmitString(uint16_t u16size)
{
	//	HAL_UART_Transmit_IT(&HAL_Uart1, USART1TXBUFFER, sizeof(USART1TXBUFFER));	
		HAL_UART_Transmit(&HAL_UART_EVSE_Controller, USART1TXBUFFER, u16size, 5);	
}*/



void UARTEVSEController_vState(void)
{
	switch (UARTEVSE_State)
	{
	case WaitResponse:
		/* Move to CheckFirstByte if data is recieved */
		if (UARTEVSE_RxFifo.u8Rx1WriteIndex != UARTEVSE_RxFifo.u8Rx1WriteRead)
			UARTEVSE_State = CheckFirstByte;
			UARTEVSE_RxFifo.u16WaitTime = 0;
		break;
	case CheckFirstByte:
		/* Check if data is valid by confirming first bit */
		if (UARTEVSE_RxFifo.USARTEVSE_RXBUFFER[++UARTEVSE_RxFifo.u8Rx1WriteRead % USARTEVSE_RXBUFFERSIZE] == UART_START_BYTE_RX)
		{
			UARTEVSE_State = RecordIncomingData;
			UARTEVSE_RxPacket.u8Rx1StartByteBuffer = UARTEVSE_RxFifo.u8Rx1WriteRead;
		}
		else
		{
			UARTEVSE_State = IncorrectDataRecieved;
		}
		break;
	case RecordIncomingData:
		/* Record incoming data and check for stop bits */
		while (UARTEVSE_RxFifo.u8Rx1WriteIndex != (UARTEVSE_RxFifo.u8Rx1WriteRead % USARTEVSE_RXBUFFERSIZE))
		{
			if (UARTEVSE_RxFifo.USARTEVSE_RXBUFFER[++UARTEVSE_RxFifo.u8Rx1WriteRead % USARTEVSE_RXBUFFERSIZE] == UART_STOP_BYTE_RX_2)
			{
				if (UARTEVSE_RxFifo.USARTEVSE_RXBUFFER[(UARTEVSE_RxFifo.u8Rx1WriteRead - 1) % USARTEVSE_RXBUFFERSIZE] == UART_STOP_BYTE_RX_1)
				{
					UARTEVSE_State = ProcessData;
					break;
				}
			}
					
		}
		
		if (UARTEVSE_RxFifo.u16WaitTime >= UARTEVSE_RECIEVE_TIME)
		{
			// TO DO: CHANGE TO INCORRECT DATA
			UARTEVSE_State = ProcessData;
		}
		
		break;
	case ProcessData:
		/* Check CRC, (send ACK), store data, check ID and send data to appropriate terminal */
		
		/* Set up counters */
		UARTEVSE_RxPacket.u8Rx1StopByteBuffer = UARTEVSE_RxFifo.u8Rx1WriteRead % USARTEVSE_RXBUFFERSIZE;
		
		// POTENTIALLY FIX THIS UP
		UARTEVSE_RxFifo.u8Rx1WriteRead = UARTEVSE_RxPacket.u8Rx1StartByteBuffer % USARTEVSE_RXBUFFERSIZE;
		if (((UARTEVSE_RxPacket.u8Rx1StopByteBuffer - UARTEVSE_RxPacket.u8Rx1StartByteBuffer) + USARTEVSE_RXBUFFERSIZE) % USARTEVSE_RXBUFFERSIZE <= UART_MINIMUM_LENGTH)
		{
			UARTEVSE_State = IncorrectDataRecieved;
			break;
		}
		
		/* Store data in structure */
		UARTEVSE_RxPacket.USARTEVSEID[0] = UARTEVSE_RxFifo.USARTEVSE_RXBUFFER[++UARTEVSE_RxFifo.u8Rx1WriteRead % USARTEVSE_RXBUFFERSIZE];
		UARTEVSE_RxPacket.USARTEVSEID[1] = UARTEVSE_RxFifo.USARTEVSE_RXBUFFER[++UARTEVSE_RxFifo.u8Rx1WriteRead % USARTEVSE_RXBUFFERSIZE];
		
		UARTEVSE_RxPacket.USARTEVSECOMBYTE = UARTEVSE_RxFifo.USARTEVSE_RXBUFFER[++UARTEVSE_RxFifo.u8Rx1WriteRead % USARTEVSE_RXBUFFERSIZE];
		

		for (UARTEVSE_RxPacket.u8Rx1BufferLength = 0; UARTEVSE_RxPacket.u8Rx1BufferLength < USARTEVSE_RXPAYLOADSIZE; ++UARTEVSE_RxPacket.u8Rx1BufferLength)
		{
			if (UARTEVSE_RxFifo.u8Rx1WriteRead % USARTEVSE_RXBUFFERSIZE == ((UARTEVSE_RxPacket.u8Rx1StopByteBuffer + (USARTEVSE_RXBUFFERSIZE - 3)) % USARTEVSE_RXBUFFERSIZE))
			{
				break;
			}
			
			UARTEVSE_RxPacket.USARTEVSEPAYLOAD[UARTEVSE_RxPacket.u8Rx1BufferLength] = UARTEVSE_RxFifo.USARTEVSE_RXBUFFER[++UARTEVSE_RxFifo.u8Rx1WriteRead % USARTEVSE_RXBUFFERSIZE];
			
		}

		UARTEVSE_RxPacket.USARTEVSECRC = UARTEVSE_RxFifo.USARTEVSE_RXBUFFER[++UARTEVSE_RxFifo.u8Rx1WriteRead % USARTEVSE_RXBUFFERSIZE];
		
		/* Skip end bits */
		UARTEVSE_RxFifo.u8Rx1WriteRead = (UARTEVSE_RxFifo.u8Rx1WriteRead + UART_END_BIT_LENGTH) % USARTEVSE_RXBUFFERSIZE;
		
		/* Check CRC and send to relevant payment module */
		if (0)//!USARTEVSE_vCheckCRC()) 
		{
			UARTEVSE_State = IncorrectDataRecieved;
			break;
		}
		else if (UARTEVSE_RxPacket.USARTEVSECOMBYTE == UART_REQ_REPLYWITHBUFFER)
		{
			UARTEVSE_State = TransmitFromBuffer;
			break;
		}
		else if (UARTEVSE_RxPacket.USARTEVSECOMBYTE == UART_REQ_SENDTOPAYMENT)
		{
			if (UARTEVSE_Terminal1(UARTEVSE_RxPacket.USARTEVSEID))
			{
				LED_vRequestLED(blueRS232_1, on, 0, 0);
				HAL_UART_Transmit_IT(&HAL_UART_Payment1, UARTEVSE_RxPacket.USARTEVSEPAYLOAD, sizeof(UARTEVSE_RxPacket.USARTEVSEPAYLOAD[0])*UARTEVSE_RxPacket.u8Rx1BufferLength);  //sizeof(UARTEVSE_RxPacket.USARTEVSEPAYLOAD[0])*UARTEVSE_RxPacket.u8Rx1BufferLength);
			} 
			else if (UARTEVSE_Terminal2(UARTEVSE_RxPacket.USARTEVSEID))
			{
				LED_vRequestLED(blueRS232_2, on, 0, 0);
				HAL_UART_Transmit_IT(&HAL_UART_Payment2, UARTEVSE_RxPacket.USARTEVSEPAYLOAD, sizeof(UARTEVSE_RxPacket.USARTEVSEPAYLOAD[0])*UARTEVSE_RxPacket.u8Rx1BufferLength);
			}
		}
		else if(UARTEVSE_RxPacket.USARTEVSECOMBYTE == UART_REQ_RESETDEVICE)
		{
			UART_ResetBuffers();
		}

		/* Go back to wait state */
		UARTEVSE_State = WaitResponse;
		break;
	case TransmitFromBuffer:
		/* Transmit the oldest packet of data that has been stored from the payment modules */
		LED_vRequestLED(blueRS485_1, flash, 100, 100);
		UART_SendFromEVSETxBuffer(UARTEVSE_RxPacket.USARTEVSEID[1] - '0');
		UARTEVSE_State = WaitResponse;
		break;
	case IncorrectDataRecieved:
		/* Implement error case here */
		UARTEVSE_RxFifo.u8Rx1WriteRead = UARTEVSE_RxFifo.u8Rx1WriteRead % USARTEVSE_RXBUFFERSIZE;
		UARTEVSE_State = WaitResponse;
		break;
	default:
		/* Go back to wait state */
		UARTEVSE_RxFifo.u8Rx1WriteRead = UARTEVSE_RxFifo.u8Rx1WriteRead % USARTEVSE_RXBUFFERSIZE;
		UARTEVSE_State = WaitResponse;
		break;
	}
}	


void UARTPaymentTerminal1_vState(void)
{
	switch (UARTPayment1_State)
	{
	case WaitResponse:
		/* Move to RecordIncomingData if data is recieved */
		if (UARTPayment1_RxFifo.u16Rx1WriteIndex != UARTPayment1_RxFifo.u16Rx1WriteRead)
		{
			UARTPayment1_State = RecordIncomingData;
			UARTPayment1_RxFifo.u16WaitTime = 0;
		}
		break;
	case RecordIncomingData:
		/* Wait for entire packet to be recieved. Wait time or look for last byte */
		//while (UARTPayment1_RxFifo.u16Rx1WriteIndex != UARTPayment1_RxFifo.u16Rx1CheckIndex)
		//{
			if (UARTPayment1_RxFifo.u16WaitTime > UART_PAYMENT1_RECIEVE_TIME || UARTPayment1_LastByte() || UARTPayment1_ACKByte())
			{
				UARTPayment1_State = ProcessData;
				break;
			}
		//	UARTPayment1_RxFifo.u16Rx1CheckIndex = (UARTPayment1_RxFifo.u16Rx1CheckIndex + 1) % USARTPAYMENT1_RXBUFFERSIZE;
		//}
		break;
	case ProcessData:
		/* Add start, stop, ID and CRC  */
		UARTPayment1_TxPacket.u16Tx1BufferLength = 0;
		
		/* Add start byte */
		UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD[UARTPayment1_TxPacket.u16Tx1BufferLength++] = UART_START_BYTE_TX;
		
		/* Add ID */
		UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD[UARTPayment1_TxPacket.u16Tx1BufferLength++] = PAYMENT_TERMINAL_ID_1_BYTE_0;
		UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD[UARTPayment1_TxPacket.u16Tx1BufferLength++] = PAYMENT_TERMINAL_ID_1_BYTE_1;
		
		/* Add Control Byte */
		UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD[UARTPayment1_TxPacket.u16Tx1BufferLength++] = UART_REQ_REPLYWITHBUFFER;
		
		/* Add Message */
		for (; UARTPayment1_TxPacket.u16Tx1BufferLength < USARTPAYMENT1_TXPAYLOADSIZE; UARTPayment1_TxPacket.u16Tx1BufferLength++)
		{
			/* All message added when all of buffer read */
			if (UARTPayment1_RxFifo.u16Rx1WriteRead  == UARTPayment1_RxFifo.u16Rx1WriteIndex || UARTPayment1_InsertedTxLastByte() || UARTPayment1_InsertedTxACKByte())
			{
				break;
			}
			UARTPayment1_RxFifo.u16Rx1WriteRead = (UARTPayment1_RxFifo.u16Rx1WriteRead+1) % USARTPAYMENT1_RXBUFFERSIZE;
			UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD[UARTPayment1_TxPacket.u16Tx1BufferLength] = UARTPayment1_RxFifo.USARTPAYMENT1_RXBUFFER[UARTPayment1_RxFifo.u16Rx1WriteRead];
		}
		
		/* Add CRC byte */
		UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD[UARTPayment1_TxPacket.u16Tx1BufferLength++]=USARTPayment_vCalcCRC(UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD, UARTPayment1_TxPacket.u16Tx1BufferLength);
		
		/* Add stop bytes */
		UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD[UARTPayment1_TxPacket.u16Tx1BufferLength++] = UART_STOP_BYTE_TX_1;
		UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD[UARTPayment1_TxPacket.u16Tx1BufferLength++] = UART_STOP_BYTE_TX_2;
		
		UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD[UARTPayment1_TxPacket.u16Tx1BufferLength] = 0x00;
		
		/* Send message to EVSE */
		//LED_vRequestLED(blueRS485_1, flash, 100, 100);
		//HAL_UART_Transmit_IT(&HAL_UART_EVSE_Controller, UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD, sizeof(UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD[0])*UARTPayment1_TxPacket.u8Tx1BufferLength);
		UART_AddToEVSETxBuffer(UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD, UARTPayment1_TxPacket.u16Tx1BufferLength, PAYMENT_TERMINAL_ID_1);
		
		UARTPayment1_State = WaitResponse;
		break;
	default:
		UARTPayment1_State = WaitResponse;
		break;
	}

}	

void UARTPaymentTerminal2_vState(void)
{
	switch (UARTPayment2_State)
	{
	case WaitResponse:
		/* Move to RecordIncomingData if data is recieved */
		if (UARTPayment2_RxFifo.u16Rx2WriteIndex != UARTPayment2_RxFifo.u16Rx2WriteRead)
		{
			UARTPayment2_State = RecordIncomingData;
			UARTPayment2_RxFifo.u16WaitTime = 0;
		}
		break;
	case RecordIncomingData:
		/* Wait for entire packet to be recieved */
		while (UARTPayment2_RxFifo.u16Rx2WriteIndex != UARTPayment2_RxFifo.u16Rx2CheckIndex)
		{
			if (UARTPayment2_RxFifo.u16WaitTime > UART_PAYMENT2_RECIEVE_TIME || UARTPayment2_LastByte() || UARTPayment2_ACKByte())
			{
				UARTPayment2_State = ProcessData;
				break;
			}
			UARTPayment2_RxFifo.u16Rx2CheckIndex = (UARTPayment2_RxFifo.u16Rx2CheckIndex + 1) % USARTPAYMENT2_RXBUFFERSIZE;
		}
		break;
	case ProcessData:
		/* Add start, stop, ID and CRC  */
		UARTPayment2_TxPacket.u16Tx2BufferLength = 0;
		
		/* Add start byte */
		UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[UARTPayment2_TxPacket.u16Tx2BufferLength++] = UART_START_BYTE_TX;
		
		/* Add ID */
		UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[UARTPayment2_TxPacket.u16Tx2BufferLength++] = PAYMENT_TERMINAL_ID_2_BYTE_0;
		UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[UARTPayment2_TxPacket.u16Tx2BufferLength++] = PAYMENT_TERMINAL_ID_2_BYTE_1;
		
		/* Add Control Byte */
		UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD[UARTPayment1_TxPacket.u16Tx1BufferLength++] = UART_REQ_REPLYWITHBUFFER;
		
		/* Add Message */
		for (; UARTPayment2_TxPacket.u16Tx2BufferLength < USARTPAYMENT2_TXPAYLOADSIZE; UARTPayment2_TxPacket.u16Tx2BufferLength++)
		{
			/* All message added when all of buffer read */
			if (UARTPayment2_RxFifo.u16Rx2WriteRead  == UARTPayment2_RxFifo.u16Rx2WriteIndex || UARTPayment2_InsertedTxLastByte() || UARTPayment2_InsertedTxACKByte())
			{
				break;
			}
			UARTPayment2_RxFifo.u16Rx2WriteRead = (++UARTPayment2_RxFifo.u16Rx2WriteRead) % USARTPAYMENT2_RXBUFFERSIZE;
			UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[UARTPayment2_TxPacket.u16Tx2BufferLength] = UARTPayment2_RxFifo.USARTPAYMENT2_RXBUFFER[UARTPayment2_RxFifo.u16Rx2WriteRead];
		}
		
		/* Add CRC byte */
		UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[UARTPayment2_TxPacket.u16Tx2BufferLength++] = USARTPayment_vCalcCRC(UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD, UARTPayment2_TxPacket.u16Tx2BufferLength);
		
		/* Add stop bytes */
		UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[UARTPayment2_TxPacket.u16Tx2BufferLength++] = UART_STOP_BYTE_TX_1;
		UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[UARTPayment2_TxPacket.u16Tx2BufferLength++] = UART_STOP_BYTE_TX_2;
		
		UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[UARTPayment2_TxPacket.u16Tx2BufferLength] = 0x00;
		/* Send message to EVSE */
		//UARTEVSE_vTransmitMode();
		//LED_vRequestLED(blueRS485_1, flash, 100, 100);
		//HAL_UART_Transmit_IT(&HAL_UART_EVSE_Controller, UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD, sizeof(UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[0])*UARTPayment2_TxPacket.u8Tx2BufferLength);
		UART_AddToEVSETxBuffer(UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD, UARTPayment2_TxPacket.u16Tx2BufferLength, PAYMENT_TERMINAL_ID_2);
		
		UARTPayment2_State = WaitResponse;
		break;
	default:
		UARTPayment2_State = WaitResponse;
		break;
	}

}	

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *haurt)
{
	/* Called when data reception is complete */
	/* Place byte on buffer into ring buffer and increment circular buffer */
	int u8i = 0;
	
	
	if (UARTEVSE_Mode != TransmitData)
	{
		/* For EVSE Controller */
		if (haurt->Instance == HAL_UART_EVSE_Controller.Instance)
		{
			UARTEVSE_RxFifo.u8Rx1WriteIndex = ++UARTEVSE_RxFifo.u8Rx1WriteIndex % USARTEVSE_RXBUFFERSIZE;
			UARTEVSE_RxFifo.USARTEVSE_RXBUFFER[UARTEVSE_RxFifo.u8Rx1WriteIndex] = USARTEVSE_TEMPBUFF[u8i];
			
		}
	}
		
	/* For Payment Terminal 1 */
	if (haurt->Instance == HAL_UART_Payment1.Instance)
	{
		/* Prevent overwriting unused data */
		if ((UARTPayment1_RxFifo.u16Rx1WriteIndex + 1) % USARTPAYMENT1_RXBUFFERSIZE != UARTPayment1_RxFifo.u16Rx1WriteRead)
		{
			UARTPayment1_RxFifo.u16Rx1WriteIndex = (UARTPayment1_RxFifo.u16Rx1WriteIndex+1) % USARTPAYMENT1_RXBUFFERSIZE;
			UARTPayment1_RxFifo.USARTPAYMENT1_RXBUFFER[UARTPayment1_RxFifo.u16Rx1WriteIndex] = USARTPAYMENT1_TEMPBUFF[u8i];
		}
	}
		
	/* For Payment Terminal 2 */
	if (haurt->Instance == HAL_UART_Payment2.Instance)
	{
		/* Prevent overwriting unused data */
		if ((UARTPayment2_RxFifo.u16Rx2WriteIndex + 1) % USARTPAYMENT2_RXBUFFERSIZE != UARTPayment2_RxFifo.u16Rx2WriteRead)
		{	
			UARTPayment2_RxFifo.u16Rx2WriteIndex = ++UARTPayment2_RxFifo.u16Rx2WriteIndex % USARTPAYMENT2_RXBUFFERSIZE;
			UARTPayment2_RxFifo.USARTPAYMENT2_RXBUFFER[UARTPayment2_RxFifo.u16Rx2WriteIndex] = USARTPAYMENT2_TEMPBUFF[u8i];
		}
	}
	
		
	// MAKE SURE NOT OVERWRITING DATA, CHECK READ AND WRITE INDEX
	
}


void HAL_UART_TxCpltCallback(UART_HandleTypeDef *haurt)
{
	/* Called when data transmission is complete */
	
	if(haurt->Instance == HAL_UART_EVSE_Controller.Instance && UARTEVSE_Mode == TransmitData)
	{
		UARTEVSE_vRecieveMode();
		HAL_UART_Receive_IT(&HAL_UART_EVSE_Controller, (uint8_t *)USARTEVSE_TEMPBUFF, 1);
	}
	
	if (haurt->Instance == HAL_UART_Payment1.Instance)
	{
		LED_vRequestLED(blueRS232_1, off, 0, 0);
		HAL_UART_Receive_IT(&HAL_UART_Payment1, (uint8_t *)USARTPAYMENT1_TEMPBUFF, 1);
	}
	
	if (haurt->Instance == HAL_UART_Payment2.Instance)
	{
		LED_vRequestLED(blueRS232_2, off, 0, 0);
		HAL_UART_Receive_IT(&HAL_UART_Payment2, (uint8_t *)USARTPAYMENT2_TEMPBUFF, 1);
	}
}


uint8_t USARTEVSE_vCheckCRC(void)
{
	uint16_t counter = UARTEVSE_RxPacket.u8Rx1StartByteBuffer;
	uint8_t USART1CRCCalc = UARTEVSE_RxFifo.USARTEVSE_RXBUFFER[counter++];
	
	for ( ; counter != (UARTEVSE_RxPacket.u8Rx1StopByteBuffer + (USARTEVSE_RXBUFFERSIZE - UART_END_BIT_LENGTH)) % USARTEVSE_RXBUFFERSIZE; counter = (counter + 1) % USARTEVSE_RXBUFFERSIZE)
	{
		USART1CRCCalc ^= UARTEVSE_RxFifo.USARTEVSE_RXBUFFER[counter];
	}
	
		
	return (USART1CRCCalc == UARTEVSE_RxPacket.USARTEVSECRC);
}

uint8_t USARTPayment_vCalcCRC(uint8_t* u8MessageBuffer, uint16_t length)
{
	uint16_t counter = 0;
	uint8_t USARTCRCCalc = u8MessageBuffer[counter++];
	
	for ( ; counter < length; counter++)
	{
		USARTCRCCalc ^= u8MessageBuffer[counter];
	}
	
		
	return USARTCRCCalc;
}

void UART_vWaitTimeUpdate(void)
{
	UARTEVSE_RxFifo.u16WaitTime++;
	UARTPayment1_RxFifo.u16WaitTime++;
	UARTPayment2_RxFifo.u16WaitTime++;
}

void UARTEVSE_vRecieveMode(void)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	UARTEVSE_Mode = RecieveData;
}

void UARTEVSE_vTransmitMode(void)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
	UARTEVSE_Mode = TransmitData;
}

uint8_t UARTEVSE_Terminal1(uint8_t ID[2]) 
{
	return (ID[0] == PAYMENT_TERMINAL_ID_1_BYTE_0 && ID[1] == PAYMENT_TERMINAL_ID_1_BYTE_1);
}

uint8_t UARTEVSE_Terminal2(uint8_t ID[2]) 
{
	return (ID[0] == PAYMENT_TERMINAL_ID_2_BYTE_0 && ID[1] == PAYMENT_TERMINAL_ID_2_BYTE_1);
}

void UART_AddToEVSETxBuffer(uint8_t PAYLOAD[], uint16_t length, uint8_t u8ID)
{
	/* Add a packet from the payment module onto the buffer to be sent to the EVSE Controller */
	if (u8ID == PAYMENT_TERMINAL_ID_1)
	{
		if (UART_CheckOverWriteEVSETxBuffer(u8ID))
		{
			/* Only enter data if it will not overwrite data that has not yet been sent */
			UART_u8intcpy(UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Buffer[UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Store].UARTPAYLOAD, PAYLOAD, length); 
			UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Buffer[UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Store].u8BufferLength = length;
	
			UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Store = (UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Store + 1) % UART_EVSE_NUMOFPACKETSTXBUFFER;
		}
	}
	else if (u8ID == PAYMENT_TERMINAL_ID_2)
	{
		if (UART_CheckOverWriteEVSETxBuffer(u8ID))
		{
			/* Only enter data if it will not overwrite data that has not yet been sent */
			UART_u8intcpy(UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Buffer[UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Store].UARTPAYLOAD, PAYLOAD, length); 
			UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Buffer[UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Store].u8BufferLength = length;
	
			UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Store = (UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Store + 1) % UART_EVSE_NUMOFPACKETSTXBUFFER;
		}
	}
	
}

uint8_t UART_CheckOverWriteEVSETxBuffer(uint8_t u8ID) 
{
	if (u8ID == PAYMENT_TERMINAL_ID_1)
	{
		return (UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Store + 1) % UART_EVSE_NUMOFPACKETSTXBUFFER != UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Sent;
	}
	else if (u8ID == PAYMENT_TERMINAL_ID_2) 
	{
		return (UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Store + 1) % UART_EVSE_NUMOFPACKETSTXBUFFER != UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Sent;
	}
	else
	{
		return 0;
	}
}

void UART_SendFromEVSETxBuffer(uint8_t u8ID)
{
	/* Used to send packets from the EVSE Controller buffer */
	if (u8ID == PAYMENT_TERMINAL_ID_1)
	{
		if (UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Store == UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Sent)
		{
			/* If no data on buffer, send ESC control message */
			emptyBufferMessage[2] = '0' + u8ID;

			emptyBufferMessage[4] = USARTPayment_vCalcCRC(emptyBufferMessage, 4); 
			emptyBufferMessage[5] = UART_STOP_BYTE_TX_1;
			emptyBufferMessage[6] = UART_STOP_BYTE_TX_2;
		
			UART_EVSESendMessage(emptyBufferMessage, UART_EMPTYBUFFER_MESLEN);
		}
		else
		{
			UART_EVSESendMessage(UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Buffer[UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Sent].UARTPAYLOAD, UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Buffer[UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Sent].u8BufferLength); 
			UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Sent = (UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Sent + 1) % UART_EVSE_NUMOFPACKETSTXBUFFER;
		}
	}
	else if (u8ID == PAYMENT_TERMINAL_ID_2)
	{
		if (UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Store == UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Sent)
		{
			/* If no data on buffer, send ESC control message */
			emptyBufferMessage[2] = '0' + u8ID;

			emptyBufferMessage[4] = USARTPayment_vCalcCRC(emptyBufferMessage, 4); 
			emptyBufferMessage[5] = UART_STOP_BYTE_TX_1;
			emptyBufferMessage[6] = UART_STOP_BYTE_TX_2;
		
			UART_EVSESendMessage(emptyBufferMessage, UART_EMPTYBUFFER_MESLEN);
		}
		else
		{
			UART_EVSESendMessage(UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Buffer[UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Sent].UARTPAYLOAD, UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Buffer[UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Sent].u8BufferLength); 
			UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Sent = (UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Sent + 1) % UART_EVSE_NUMOFPACKETSTXBUFFER;
		}
	}
}


void UART_u8intcpy(uint8_t dest[], uint8_t loc[], uint16_t length) 
{
	uint16_t u16i=0;
	/*
	while (loc[u16i] != 0x00)
	{
		dest[u16i] = loc[u16i];
		u16i++;
	}
	*/
	for (; u16i < length; u16i++)
	{
		dest[u16i] = loc[u16i];
	}
}


void UART_EVSESendMessage(uint8_t *message, uint16_t length) 
{
	//HAL_Delay(10);
	// TO REMOVE ^
	UARTEVSE_vTransmitMode();
	HAL_UART_Transmit_IT(&HAL_UART_EVSE_Controller, message, sizeof(message[0])*length);
}
		
uint16_t UART_MessageLength(uint8_t *message) 
{
	uint16_t length=0;
	
	while (message[length] != 0x00)
	{
		length++;
	}
	
	return length;
}



uint8_t UARTPayment1_LastByte(void) 
{
	uint8_t temp1 = UARTPayment1_RxFifo.USARTPAYMENT1_RXBUFFER[(UARTPayment1_RxFifo.u16Rx1WriteIndex + USARTPAYMENT1_RXBUFFERSIZE - 2) % USARTPAYMENT1_RXBUFFERSIZE];
	uint8_t temp2 = UARTPayment1_RxFifo.USARTPAYMENT1_RXBUFFER[(UARTPayment1_RxFifo.u16Rx1WriteIndex + USARTPAYMENT1_RXBUFFERSIZE - 3) % USARTPAYMENT1_RXBUFFERSIZE];
	return temp1 == ADVAM_LAST_BYTE && (temp2 != ADVAM_START_BYTE && temp2 != ADVAM_ACK_BYTE && temp2 != ADVAM_WACK_BYTE);
}

uint8_t UARTPayment1_ACKByte(void) 
{
	uint8_t temp3 = UARTPayment1_RxFifo.USARTPAYMENT1_RXBUFFER[(UARTPayment1_RxFifo.u16Rx1WriteIndex + USARTPAYMENT1_RXBUFFERSIZE - 2) % USARTPAYMENT1_RXBUFFERSIZE];
	uint8_t temp1 = UARTPayment1_RxFifo.USARTPAYMENT1_RXBUFFER[(UARTPayment1_RxFifo.u16Rx1WriteIndex + USARTPAYMENT1_RXBUFFERSIZE - 1) % USARTPAYMENT1_RXBUFFERSIZE];
	uint8_t temp2 = UARTPayment1_RxFifo.USARTPAYMENT1_RXBUFFER[UARTPayment1_RxFifo.u16Rx1WriteIndex];
	
	return (temp1 == ADVAM_ACK_BYTE && (temp3 != ADVAM_START_BYTE && temp3 != ADVAM_ACK_BYTE && temp3 != ADVAM_WACK_BYTE)) || (temp2 == ADVAM_NAK_BYTE && (temp1 != ADVAM_START_BYTE && temp1 != ADVAM_ACK_BYTE && temp1 != ADVAM_WACK_BYTE));
}

uint8_t UARTPayment1_InsertedTxLastByte(void) 
{
	uint8_t temp1 = UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD[UARTPayment1_TxPacket.u16Tx1BufferLength - 3];
	uint8_t temp2 = UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD[UARTPayment1_TxPacket.u16Tx1BufferLength - 4];
	return temp1 == ADVAM_LAST_BYTE && (temp2 != ADVAM_START_BYTE && temp2 != ADVAM_ACK_BYTE && temp2 != ADVAM_WACK_BYTE);
}

uint8_t UARTPayment1_InsertedTxACKByte(void) 
{
	uint8_t temp1 = UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD[UARTPayment1_TxPacket.u16Tx1BufferLength - 3];
	uint8_t temp2 = UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD[UARTPayment1_TxPacket.u16Tx1BufferLength - 2];
	uint8_t temp3 = UARTPayment1_TxPacket.USARTPAYMENT1PAYLOAD[UARTPayment1_TxPacket.u16Tx1BufferLength - 1];
	return (temp2 == ADVAM_ACK_BYTE && (temp1 != ADVAM_START_BYTE && temp1 != ADVAM_ACK_BYTE && temp1 != ADVAM_WACK_BYTE)) || (temp3 == ADVAM_NAK_BYTE && (temp2 != ADVAM_START_BYTE && temp2 != ADVAM_ACK_BYTE && temp2 != ADVAM_WACK_BYTE));
}


uint8_t UARTPayment2_LastByte(void) 
{
	uint8_t temp1 = UARTPayment2_RxFifo.USARTPAYMENT2_RXBUFFER[(UARTPayment2_RxFifo.u16Rx2WriteIndex + USARTPAYMENT2_RXBUFFERSIZE - 2) % USARTPAYMENT2_RXBUFFERSIZE];
	uint8_t temp2 = UARTPayment2_RxFifo.USARTPAYMENT2_RXBUFFER[(UARTPayment2_RxFifo.u16Rx2WriteIndex + USARTPAYMENT2_RXBUFFERSIZE - 3) % USARTPAYMENT2_RXBUFFERSIZE];
	return temp1 == ADVAM_LAST_BYTE && (temp2 != ADVAM_START_BYTE && temp2 != ADVAM_ACK_BYTE && temp2 != ADVAM_WACK_BYTE);
}

uint8_t UARTPayment2_ACKByte(void) 
{
	uint8_t temp3 = UARTPayment2_RxFifo.USARTPAYMENT2_RXBUFFER[(UARTPayment2_RxFifo.u16Rx2WriteIndex + USARTPAYMENT1_RXBUFFERSIZE - 2) % USARTPAYMENT1_RXBUFFERSIZE];
	uint8_t temp1 = UARTPayment2_RxFifo.USARTPAYMENT2_RXBUFFER[(UARTPayment2_RxFifo.u16Rx2WriteIndex + USARTPAYMENT2_RXBUFFERSIZE - 1) % USARTPAYMENT2_RXBUFFERSIZE];
	uint8_t temp2 = UARTPayment2_RxFifo.USARTPAYMENT2_RXBUFFER[UARTPayment2_RxFifo.u16Rx2WriteIndex];
	return (temp1 == ADVAM_ACK_BYTE && (temp3 != ADVAM_START_BYTE && temp3 != ADVAM_ACK_BYTE && temp3 != ADVAM_WACK_BYTE)) || (temp2 == ADVAM_NAK_BYTE && (temp1 != ADVAM_START_BYTE && temp1 != ADVAM_ACK_BYTE && temp1 != ADVAM_WACK_BYTE));
}

uint8_t UARTPayment2_InsertedTxLastByte(void) 
{
	uint8_t temp1 = UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[UARTPayment2_TxPacket.u16Tx2BufferLength - 3];
	uint8_t temp2 = UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[UARTPayment2_TxPacket.u16Tx2BufferLength - 4];
	return temp1 == ADVAM_LAST_BYTE && (temp2 != ADVAM_START_BYTE && temp2 != ADVAM_ACK_BYTE && temp2 != ADVAM_WACK_BYTE);
}

uint8_t UARTPayment2_InsertedTxACKByte(void) 
{
	uint8_t temp1 = UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[UARTPayment2_TxPacket.u16Tx2BufferLength - 3];
	uint8_t temp2 = UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[UARTPayment2_TxPacket.u16Tx2BufferLength - 2];
	uint8_t temp3 = UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[UARTPayment2_TxPacket.u16Tx2BufferLength - 1];
	return (temp2 == ADVAM_ACK_BYTE && (temp1 != ADVAM_START_BYTE && temp1 != ADVAM_ACK_BYTE && temp1 != ADVAM_WACK_BYTE)) || (temp3 == ADVAM_NAK_BYTE && (temp2 != ADVAM_START_BYTE && temp2 != ADVAM_ACK_BYTE && temp2 != ADVAM_WACK_BYTE));
}

/*
uint8_t UARTPayment2_LastByte(void) 
{
	uint8_t temp = UARTPayment2_RxFifo.USARTPAYMENT2_RXBUFFER[(UARTPayment2_RxFifo.u16Rx2WriteIndex + USARTEVSE_RXBUFFERSIZE - 2) % USARTEVSE_RXBUFFERSIZE];
	return temp == ADVAM_LAST_BYTE;// || temp == ADVAM_ACK_BYTE || temp == ADVAM_NAK_BYTE;
}

uint8_t UARTPayment2_ACKByte(void) 
{
	uint8_t temp = UARTPayment2_RxFifo.USARTPAYMENT2_RXBUFFER[(UARTPayment2_RxFifo.u16Rx2WriteIndex + USARTEVSE_RXBUFFERSIZE - 1) % USARTEVSE_RXBUFFERSIZE];
	return  temp == ADVAM_ACK_BYTE || temp == ADVAM_NAK_BYTE;
}

uint8_t UARTPayment2_InsertedTxLastByte(void) 
{
	uint8_t temp = UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[UARTPayment2_TxPacket.u16Tx2BufferLength - 3];
	return temp == ADVAM_LAST_BYTE;// || temp == ADVAM_ACK_BYTE || temp == ADVAM_NAK_BYTE;
}

uint8_t UARTPayment2_InsertedTxACKByte(void) 
{
	uint8_t temp1 = UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[UARTPayment2_TxPacket.u16Tx2BufferLength - 2];
	uint8_t temp2 = UARTPayment2_TxPacket.USARTPAYMENT2PAYLOAD[UARTPayment2_TxPacket.u16Tx2BufferLength - 1];
	return temp1 == ADVAM_ACK_BYTE || temp2 == ADVAM_NAK_BYTE;
}
*/

void UART_ResetBuffers(void) 
{
	uint8_t u16i;
	if (UARTEVSE_Terminal1(UARTEVSE_RxPacket.USARTEVSEID))
	{
		for (u16i = 0; u16i < UART_EVSE_NUMOFPACKETSTXBUFFER; u16i++)
		{
			UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Buffer[u16i].UARTPAYLOAD[0] = 0x00;
			UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Buffer[u16i].u8BufferLength = 0x00;
		}
		UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Store = 0;
		UART_EVSETxBuffer_Term1.UART_EVSETxPacket_Sent = 0;
	} 
	else if (UARTEVSE_Terminal2(UARTEVSE_RxPacket.USARTEVSEID))
	{
		for (u16i = 0; u16i < UART_EVSE_NUMOFPACKETSTXBUFFER; u16i++)
		{
			UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Buffer[u16i].UARTPAYLOAD[0] = 0x00;
			UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Buffer[u16i].u8BufferLength = 0x00;
		}
		UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Store = 0;
		UART_EVSETxBuffer_Term2.UART_EVSETxPacket_Sent = 0;
	}
}