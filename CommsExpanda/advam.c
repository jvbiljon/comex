#include "advam.h"


uint8_t ADVAM_TXBUFFER[ADVAM_TXBUFFER_SIZE];
ADVAM_TxPayload_t ADVAM_TxPayload;
char ADVAM_lengthInASCII[3];

ADVAM_SeqNum_t ADVAM_SeqNum = ADVAM_SEQNUM_INIT;
ADVAM_State_t ADVAM_State;
ADVAM_RxPacket_t ADVAM_LastRxPacket;
ADVAM_TxPacket_t ADVAM_TxPacket;

ADVAM_CardData_t ADVAM_CurrentCard;

/* Imported buffers from uart.c */
extern UARTEVSE_tstRxFifo UARTEVSE_RxFifo;
//extern UARTEVSE_RxPacket_t UARTEVSE_RxPacket;

void ADVAM_vState(void)
{
	
	switch (ADVAM_State.ADVAM_StateMain)
	{
	case WaitForCard:
		if (ADVAM_vCheckCard() == 1)
		{
			ADVAM_SetMainState(PreAuth);
		}
		break;
	case PreAuth: 
		// TO DO: ADD THIS OR CHANGE FROM TKT to PREAUTH START
		ADVAM_SetMainState(StartCharge);
		break;
	case StartCharge: 
		// TO DO: Add WiFi / Chargefox start charge stuff here
		LED_vRequestLED(blueRS232_1, flash, 900, 300);
		ADVAM_SetMainState(Charging);
		break;
	case Charging: 
		// TO DO: Add WiFi / Chargefox start charge stuff here
		ADVAM_SetMainState(FinishCharge);
		break;
	case FinishCharge: 
		// TO DO: Clear out ADVAM_RxPacket_t so no data is carried across
		break;
	default:
		ADVAM_SetMainState(WaitForCard);
		break;
	}
}


void ADVAM_vSendMessage(char data[], uint16_t u16size)
{
	uint16_t u16counter = ADVAM_START_BYTE_POS;
	uint16_t u16CRC_Bytes;
	
	/* Start bytes for message */
	ADVAM_TXBUFFER[u16counter++] = ADVAM_START_BYTE;
	ADVAM_TXBUFFER[u16counter++] = ADVAM_SeqNum++;
	
	/* Payload for message */
	for (; u16counter < u16size + ADVAM_HEADER_SIZE; u16counter++)
	{
		ADVAM_TXBUFFER[u16counter] = (uint16_t) data[u16counter - ADVAM_HEADER_SIZE];
	}
	
	/* Stop byte for message */
	ADVAM_TXBUFFER[u16counter++] = ADVAM_END_BYTE;
	
	/* CRC bytes for message */
	u16CRC_Bytes = ADVAM_GetCRC16(&ADVAM_TXBUFFER[1], u16counter-1, 0);
	ADVAM_TXBUFFER[u16counter++] = (u16CRC_Bytes & 0XFF);
	ADVAM_TXBUFFER[u16counter++] = (u16CRC_Bytes>>8);
	
	/* Send message over RS485 */
	UARTEVSE_vTransmitMode();
	HAL_UART_Transmit_IT(&HAL_UART_EVSE_Controller, ADVAM_TXBUFFER, u16counter);
}



void ADVAM_CRCsetPoly(unsigned short poly) {
	/* Used for setting up CRC */
	unsigned char j, ch;
	unsigned short i, check, check2;
	for (i = 0; i < 256; i++) {
		check = 0;
		ch = (unsigned char) i;
		for (j = 0; j < 8; j++) {
			if (check & 0x8000) {
				check <<= 1;
				if (!(ch & 1))
					check ^= poly;
			}
			else {
				check <<= 1;
				if (ch & 1)
					check ^= poly;
			}
			;
			ch >>= 1;
		}
		;
		check2 = 0;
		for (j = 0; j < 16; j++) {
			ch = check & 1;
			check >>= 1;
			check2 <<= 1;
			check2 |= ch;
		}
		;
		ADVAM_CRCTab[i] = check2;
	}
	;
}

uint16_t ADVAM_GetCRC16(const void *mem, int length, uint16_t crc) {
	/* Calculate CRC for ADVAM messages */
	unsigned char *block = (unsigned char *) mem;
	int i;
	for (i = 0; i < length; i++) {
		crc = (crc >> 8) ^ ADVAM_CRCTab[(block[i] ^ crc) & 0xff];
	}
	;
	return (crc);
}

uint8_t ADVAM_vCheckCard(void)
{
	// TO DO: IMPLEMENT IF IN STATE FOR TOO LONG
	switch (ADVAM_State.ADVAM_StateWaitForCard)
	{
	case LinkReq:
		if (ADVAM_LinkReq() == ADVAM_SUCCESSFUL)
		{
			ADVAM_SetWaitForCardState(DispPresentCard);
		}
		break;
	case DispPresentCard:
		// TO DO: if option is avaliable
		ADVAM_SetWaitForCardState(CheckCardDet);
		break;
	case CheckCardDet:
		if (ADVAM_CheckCardDet() == ADVAM_SUCCESSFUL)
		{
			return ADVAM_SUCCESSFUL;
		}
		else if (ADVAM_State.ADVAM_StateWaitForCard_Timeout >= ADVAM_CARDDET_TIMEOUT)
		{
			// TO DO: Re add // ADVAM_SetWaitForCardState(LinkReq);
		}
		break;
	}
	
	return ADVAM_UNSUCCESSFUL;
}


uint8_t ADVAM_LinkReq(void)
{
	switch (ADVAM_State.ADVAM_StateLinkReq)
	{
	case SendLinkReq:
		/* Send Link Request */
		ADVAM_vSendLinkReq();
		ADVAM_SetLinkReqState(CheckLinkReqACK);
		break;
	case CheckLinkReqACK:
		ADVAM_UART_RxACK();
		if (ADVAM_ACKRecieved())
		{
			ADVAM_SetLinkReqState(CheckLinkRes);
		}
		else if (ADVAM_NAKRecieved())
		{
			// ADVAM_SetLinkReqState(SendLinkReq); // IMPLEMENT AN ERROR CASE OR REPEAT
			// TO DO: IMPLEMENT A TIMEOUT
			ADVAM_State.ADVAM_StateLinkReq_Timeout = 0;
		}
		break;
	case CheckLinkRes:
		/* Check Link Response */
		ADVAM_UART_RxDataPacket();
		if (ADVAM_vCheckLinkRes() == ADVAM_SUCCESSFUL || ADVAM_State.ADVAM_StateLinkReq_Timeout >= ADVAM_TKTREADREQ_ACK_TIMEOUT)
		{
			/* Either recieve response or no response for 1 second */
			ADVAM_SetRxPacketRead();
			return ADVAM_SUCCESSFUL;
		}
		break;
	default:
		break;
	}
	return ADVAM_UNSUCCESSFUL;
}



uint8_t ADVAM_CheckCardDet(void)
{
	switch (ADVAM_State.ADVAM_StateCheckCardDet)
	{
	case SendCardDet:
		ADVAM_SendTktReadReq();
		ADVAM_State.ADVAM_StateWaitForCard_Timeout = 0;
		ADVAM_SetCheckCardDetState(CheckCardDetACK);
		break;
	case CheckCardDetACK:
		ADVAM_UART_RxACK();
		if (ADVAM_ACKRecieved())
		{
			ADVAM_SetCheckCardDetState(CheckCardDetRes);
		}
		else if (ADVAM_NAKRecieved() || ADVAM_State.ADVAM_StateWaitForCard_Timeout > ADVAM_TKTREADREQ_ACK_TIMEOUT)
		{
			ADVAM_SetCheckCardDetState(SendCardDet);
			// TO DO: IMPLEMENT A TIMEOUT
		}
		break;
	case CheckCardDetRes:
		/* Check Link Response */
		ADVAM_UART_RxDataPacket();
		ADVAM_ClearDisplayRequests();
		if (ADVAM_vCheckCardDetRes() == ADVAM_SUCCESSFUL)
		{
			return ADVAM_SUCCESSFUL;
		}
		break;
	}
	return ADVAM_UNSUCCESSFUL;
}

void ADVAM_vSendLinkReq(void)
{
	strcpy(ADVAM_TxPacket.MSGID, "LINK_REQ");
	ADVAM_vSendTxPacket();
}

void ADVAM_SendTktReadReq(void)
{
	strcpy(ADVAM_TxPacket.MSGID, "TKTREAD_REQ");
	strcpy(ADVAM_TxPacket.TMOUT, "60");
	ADVAM_vSendTxPacket();
}

void ADVAM_SendDispReq(void)
{
	strcpy(ADVAM_TxPacket.MSGID, "PMTDET_REQ");
	strcpy(ADVAM_TxPacket.TMOUT, "60");
	strcpy(ADVAM_TxPacket.AMTTX, "10.00");
	ADVAM_TxPacket.SURCH[0] = ADVAM_SURCHSET;
	ADVAM_vSendTxPacket();
}


void ADVAM_SetMainState(ADVAM_StateMain_t State)
{
	ADVAM_State.ADVAM_StateMain = State;
}

void ADVAM_SetWaitForCardState(ADVAM_StateWaitForCard_t State)
{
	ADVAM_State.ADVAM_StateWaitForCard = State;
}

void ADVAM_SetLinkReqState(ADVAM_StateLinkReq_t State)
{
	ADVAM_State.ADVAM_StateLinkReq = State;
}

void ADVAM_SetCheckCardDetState(ADVAM_StateCheckCardDet_t State)
{
	ADVAM_State.ADVAM_StateCheckCardDet = State;
}

void ADVAM_SetUARTState(ADVAM_StateUARTRx_t State)
{
	ADVAM_State.ADVAM_StateUART = State;
}

void ADVAM_SetUARTRxACKState(ADVAM_StateUARTRx_t State)
{
	ADVAM_State.ADVAM_StateUART_ACK = State;
}

uint8_t ADVAM_vCheckLinkRes(void)
{
	if (!strcmp(ADVAM_LastRxPacket.MSGID, "LINK_RES"))
	{
		ADVAM_SetRxPacketRead();
		return ADVAM_SUCCESSFUL;
	}
	else
	{
		return ADVAM_UNSUCCESSFUL;
	}
	// TO DO: If last messageID is LinkRes and ID matches with last TX then sucess
}

uint8_t ADVAM_vCheckCardDetRes(void)
{
	if (!strcmp(ADVAM_LastRxPacket.MSGID, "TKTREAD_RES"))
	{
		if (!strcmp(ADVAM_LastRxPacket.ISPAY, "N"))
		{
			// TO DO: ENSURE THIS RESETS THE MODULE BACK TO START
			return ADVAM_RESET;
		}
		else
		{
			ADVAM_RecordCardData();
		}
		ADVAM_SetRxPacketRead();
		return ADVAM_SUCCESSFUL;
	}
	else
	{
		return ADVAM_UNSUCCESSFUL;
	}
	// TO DO: If last messageID is LinkRes and ID matches with last TX then sucess
}

void ADVAM_ClearDisplayRequests(void)
{
	if (!strcmp(ADVAM_LastRxPacket.MSGID, "DISP_REQ"))
	{
		ADVAM_SetRxPacketRead();
	}
}


void ADVAM_RecordCardData(void)
{
	strcpy(ADVAM_CurrentCard.PRFIX, ADVAM_LastRxPacket.PRFIX);
	strcpy(ADVAM_CurrentCard.CRDTD, ADVAM_LastRxPacket.CRDTD);
	strcpy(ADVAM_CurrentCard.CRDHA, ADVAM_LastRxPacket.CRDHA);
}

void ADVAM_vSendTxPacket(void)
{
	char tempString[ADVAM_TXPAYLOAD_SIZE];
	char tag[10];
	
	ADVAM_TxPayload.size = 0;

	if (ADVAM_TxPacket.MSGID[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "MSGID");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.MSGID), ADVAM_TxPacket.MSGID);
		ADVAM_AddToTxPayload(tempString);
	}
	if (ADVAM_TxPacket.VERSN[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "VERSN");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.VERSN), ADVAM_TxPacket.VERSN);
		ADVAM_AddToTxPayload(tempString);
	}
	if (ADVAM_TxPacket.TMOUT[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "TMOUT");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.TMOUT), ADVAM_TxPacket.TMOUT);
		ADVAM_AddToTxPayload(tempString);
	}
	if (ADVAM_TxPacket.PRFIX[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "PRFIX");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.PRFIX), ADVAM_TxPacket.PRFIX);
		ADVAM_AddToTxPayload(tempString);
	}
	if (ADVAM_TxPacket.PRFIX[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "ISPAY");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.ISPAY), ADVAM_TxPacket.ISPAY);
		ADVAM_AddToTxPayload(tempString);
	}
	if (ADVAM_TxPacket.CRDTD[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "CRDTD");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.CRDTD), ADVAM_TxPacket.CRDTD);
		ADVAM_AddToTxPayload(tempString);
	}
	if (ADVAM_TxPacket.STATS[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "STATS");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.STATS), ADVAM_TxPacket.STATS);
		ADVAM_AddToTxPayload(tempString);
	}
	if(ADVAM_TxPacket.AMTTX[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "AMTTX");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.AMTTX), ADVAM_TxPacket.AMTTX);
		ADVAM_AddToTxPayload(tempString);
	}
	if (ADVAM_TxPacket.EQUIP[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "EQUIP");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.EQUIP), ADVAM_TxPacket.EQUIP);
		ADVAM_AddToTxPayload(tempString);
	}
	if (ADVAM_TxPacket.TXREF[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "TXREF");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.TXREF), ADVAM_TxPacket.TXREF);
		ADVAM_AddToTxPayload(tempString);
	}
	if (ADVAM_TxPacket.RCODE[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "RCODE");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.RCODE), ADVAM_TxPacket.RCODE);
		ADVAM_AddToTxPayload(tempString);
	}
	if (ADVAM_TxPacket.RTEXT[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "RTEXT");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.RTEXT), ADVAM_TxPacket.RTEXT);
		ADVAM_AddToTxPayload(tempString);
	}
	if (ADVAM_TxPacket.TSTAN[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "TSTAN");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.TSTAN), ADVAM_TxPacket.TSTAN);
		ADVAM_AddToTxPayload(tempString);
	}
	if (ADVAM_TxPacket.TAUTH[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "TAUTH");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.TAUTH), ADVAM_TxPacket.TAUTH);
		ADVAM_AddToTxPayload(tempString);
	}
	if (ADVAM_TxPacket.TCARD[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "TCARD");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.TCARD), ADVAM_TxPacket.TCARD);
		ADVAM_AddToTxPayload(tempString);
	}
	if (ADVAM_TxPacket.SCHID[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "SCHID");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.SCHID), ADVAM_TxPacket.SCHID);
		ADVAM_AddToTxPayload(tempString);
	}
	if (ADVAM_TxPacket.TAUTH[ADVAM_FIRSTBYTE] != ADVAM_ENDOFSTRING)
	{
		strcpy(tag, "TAUTH");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.TAUTH), ADVAM_TxPacket.TAUTH);
		ADVAM_AddToTxPayload(tempString);
	}
	if (ADVAM_TxPacket.SURCH[ADVAM_FIRSTBYTE] != ADVAM_SURCHRESET)
	{
		strcpy(tag, "SURCH");
		ADVAM_CombineStrings(tempString, tag, ADVAM_MessageLength(ADVAM_TxPacket.SURCH), ADVAM_TxPacket.SURCH);
		ADVAM_AddToTxPayload(tempString);
	}

	
	ADVAM_vSendMessage(ADVAM_TxPayload.BUFFER, ADVAM_TxPayload.size);
}


void ADVAM_ResetTxPacket(void)
{
	ADVAM_TxPacket.MSGID[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.VERSN[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.TMOUT[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.PRFIX[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.ISPAY[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.CRDTD[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.STATS[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.AMTTX[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.EQUIP[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.TXREF[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.RCODE[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.RTEXT[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.TSTAN[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.TAUTH[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.TCARD[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.SCHID[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.CTYPE[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_TxPacket.SURCH[ADVAM_FIRSTBYTE] = ADVAM_SURCHRESET; 
}

void ADVAM_ResetRxPacket(void)
{
	ADVAM_LastRxPacket.MSGID[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.VERSN[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.TMOUT[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.PRFIX[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.ISPAY[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.CRDTD[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.STATS[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.AMTTX[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.EQUIP[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.TXREF[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.RCODE[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.RTEXT[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.TSTAN[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.TAUTH[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.TCARD[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.SCHID[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.CTYPE[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.u8CRC[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.CRDHA[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.DISID[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.DTEXT[ADVAM_FIRSTBYTE] = ADVAM_ENDOFSTRING;
	
	ADVAM_LastRxPacket.SURCH[ADVAM_FIRSTBYTE] = ADVAM_SURCHRESET; 
	ADVAM_LastRxPacket.CONTROL = ADVAM_ENDOFSTRING;
	ADVAM_LastRxPacket.u8Rx1StartByteBuffer = 0;
	ADVAM_LastRxPacket.u8Rx1StopByteBuffer = 0;
	ADVAM_LastRxPacket.u8seqNum = 0;
	
}



void ADVAM_CombineStrings(char* combinedString, char* string1, char* string2, char* string3) 
{
	strcat(strcat(strcpy(combinedString, string1), string2), string3);
}

char* ADVAM_MessageLength(char* string) 
{
	// TO DO: NEEDS TO BE IMPROVED
	uint16_t length = strlen(string);
	/*if (length > 255)
	{
		// TO DO // return length+ASCII_OFFSET;
	}
	else if (length > 15)
	{
		ADVAM_DectoCharHex
		ADVAM_lengthInASCII[0] = '0';
		ADVAM_lengthInASCII[1] = '0';
		ADVAM_lengthInASCII[2] = ADVAM_DectoCharHex(length);
		return strcat(strcat("0", ((length & 0xF0) >> 4) + ASCII_OFFSET), ((length & 0x0F) >> 4) + ASCII_OFFSET);
	}
	else
	{
		ADVAM_lengthInASCII[0] = '0';
		ADVAM_lengthInASCII[1] = '0';
		ADVAM_lengthInASCII[2] = ADVAM_DectoCharHex(length);
		//return strcat("00", length+ASCII_OFFSET);
		return ADVAM_lengthInASCII;
	}*/
	ADVAM_lengthInASCII[0] = ADVAM_DectoCharHex((length & 0x0F00) >> 8);
	ADVAM_lengthInASCII[1] = ADVAM_DectoCharHex((length & 0x00F0) >> 4);
	ADVAM_lengthInASCII[2] = ADVAM_DectoCharHex(length & 0x000F);
	return ADVAM_lengthInASCII;
}


void ADVAM_AddToTxPayload(char* payload) 
{
	int i;
	for (i = 0; i <= ADVAM_TXPAYLOAD_SIZE & payload[i] != ADVAM_ENDOFSTRING; i++)
	{
		ADVAM_TxPayload.BUFFER[ADVAM_TxPayload.size++]=payload[i];
	}
	ADVAM_TxPayload.BUFFER[ADVAM_TxPayload.size] = ADVAM_ENDOFSTRING;
}


void ADVAM_UART_RxDataPacket(void)
{
	switch (ADVAM_State.ADVAM_StateUART)
	{
	case ADVAM_WaitResponse:
		/* Move to CheckFirstByte if data is recieved */
		if (UARTEVSE_RxFifo.u8Rx1WriteIndex != UARTEVSE_RxFifo.u8Rx1WriteRead)
		{
			ADVAM_SetUARTState(ADVAM_CheckFirstByte);
		}
		break;
	case ADVAM_CheckFirstByte:
		/* Check if data is valid by confirming first bit */
		if (ADVAM_ReadByteFromBuffer() == ADVAM_START_BYTE)
		{
			ADVAM_LastRxPacket.u8Rx1StartByteBuffer = UARTEVSE_RxFifo.u8Rx1WriteRead;
			UARTEVSE_RxFifo.u16WaitTime = 0; 
			ADVAM_SetUARTState(ADVAM_RecordIncomingData);
		}
		else
		{
			ADVAM_SetUARTState(ADVAM_WaitResponse);
			//ADVAM_SetUARTState(ADVAM_IncorrectDataRecieved);
		}
		break;
	case ADVAM_RecordIncomingData:
		/* Wait for entire packet to be recieved */
		if (UARTEVSE_RxFifo.u16WaitTime > ADVAM_PACKET_RECIEVE_TIME && ADVAM_CheckPrevRxPacketRead())
			ADVAM_SetUARTState(ADVAM_ProcessData);
		break;
	case ADVAM_ProcessData:
		ADVAM_RecordSequenceNumber();
		ADVAM_RecordPayload();
		ADVAM_RecordCRC();
		if (0) // TO DO: IMPLEMENT CRC CHECK!!! //ADVAM_CheckRxCRC() == ADVAM_BAD_CRC)
		{
			ADVAM_SetUARTState(ADVAM_IncorrectDataRecieved);
		}
		else
		{
			ADVAM_SetRxPacketUnread();
			ADVAM_SendACK();
			ADVAM_SetUARTState(ADVAM_WaitResponse);
		}
		break;
	case ADVAM_IncorrectDataRecieved:
		ADVAM_SendNAK();
		ADVAM_SetUARTState(ADVAM_WaitResponse);
		break;
	default:
		/* Go back to wait state */
		ADVAM_SetUARTState(ADVAM_WaitResponse);
		break;
	}
}	

void ADVAM_UART_RxACK(void)
{
	switch (ADVAM_State.ADVAM_StateUART_ACK)
	{
	case ADVAM_WaitResponse:
		/* Move to CheckFirstByte if data is recieved */
		if (UARTEVSE_RxFifo.u8Rx1WriteIndex != UARTEVSE_RxFifo.u8Rx1WriteRead)
		{
			UARTEVSE_RxFifo.u16WaitTime = 0; 
			ADVAM_SetUARTRxACKState(ADVAM_RecordIncomingData);
		}
		break;
	case ADVAM_RecordIncomingData:
		/* Wait for entire packet to be recieved */
		if (UARTEVSE_RxFifo.u16WaitTime > ADVAM_ACK_RECIEVE_TIME) // && ADVAM_CheckPrevRxPacketRead())
			ADVAM_SetUARTRxACKState(ADVAM_ProcessData);
		break;
	case ADVAM_ProcessData:
		ADVAM_RecordControlByte();
		ADVAM_RecordSequenceNumber();
		ADVAM_SetRxPacketUnread();
		ADVAM_SetUARTRxACKState(ADVAM_WaitResponse);
		break;
	case ADVAM_IncorrectDataRecieved:
		/* Handle error cases here */
		ADVAM_SetUARTRxACKState(ADVAM_WaitResponse);
		break;
	default:
		/* Go back to wait state */
		ADVAM_SetUARTRxACKState(ADVAM_WaitResponse);
		break;
	}
}	

void ADVAM_RecordSequenceNumber(void)
{
	ADVAM_LastRxPacket.u8seqNum = ADVAM_ReadByteFromBuffer();
}

void ADVAM_RecordControlByte(void)
{
	ADVAM_LastRxPacket.CONTROL = ADVAM_ReadByteFromBuffer();
}


void ADVAM_RecordPayload(void)
{
	ADVAM_MesTag_t messageTag;
	uint32_t messageValueLength;
	
	while (UARTEVSE_RxFifo.u8Rx1WriteIndex != (UARTEVSE_RxFifo.u8Rx1WriteRead % USARTEVSE_RXBUFFERSIZE))
	{
		messageTag = ADVAM_RecordMessageTag();
		messageValueLength = ADVAM_RecordValueLength();
		
		ADVAM_AddValueToRxPacket(messageTag, messageValueLength);
	
		if (ADVAM_NextByte() == ADVAM_END_BYTE)
		{
			ADVAM_LastRxPacket.u8Rx1StopByteBuffer = UARTEVSE_RxFifo.u8Rx1WriteRead;
			break;
		}
	}
}
		
void ADVAM_RecordCRC(void)
{
	ADVAM_LastRxPacket.u8CRC[0] = ADVAM_ReadByteFromBuffer();
	ADVAM_LastRxPacket.u8CRC[1] = ADVAM_ReadByteFromBuffer();
}

ADVAM_MesTag_t ADVAM_RecordMessageTag(void)
{
	char messageTagChar[6];
	
	uint16_t counter;
	for(counter=0; counter < ADVAM_MESSAGETAGLENGTH; counter++) 
	{
		messageTagChar[counter] = ADVAM_ReadByteFromBuffer();
	}
	messageTagChar[counter] = ADVAM_ENDOFSTRING;
	
	if (!strcmp(messageTagChar, "MSGID"))
	{
		return MSGID;
	}
	else if (!strcmp(messageTagChar, "VERSN"))
	{
		return VERSN;
	}
	else if (!strcmp(messageTagChar, "TMOUT"))
	{
		return TMOUT;
	}
	else if (!strcmp(messageTagChar, "PRFIX"))
	{
		return PRFIX;
	}
	else if (!strcmp(messageTagChar, "ISPAY"))
	{
		return ISPAY;
	}
	else if (!strcmp(messageTagChar, "STATS"))
	{
		return STATS;
	}
	else if (!strcmp(messageTagChar, "AMTTX"))
	{
		return AMTTX;
	}
	else if (!strcmp(messageTagChar, "EQUIP"))
	{
		return EQUIP;
	}
	else if (!strcmp(messageTagChar, "TXREF"))
	{
		return TXREF;
	}
	else if (!strcmp(messageTagChar, "RCODE"))
	{
		return RCODE;
	}
	else if (!strcmp(messageTagChar, "RTEXT"))
	{
		return RTEXT;
	}
	else if (!strcmp(messageTagChar, "TSTAN"))
	{
		return TSTAN;
	}
	else if (!strcmp(messageTagChar, "TAUTH"))
	{
		return TAUTH;
	}
	else if (!strcmp(messageTagChar, "TCARD"))
	{
		return TCARD;
	}
	else if (!strcmp(messageTagChar, "SCHID"))
	{
		return SCHID;
	}
	else if (!strcmp(messageTagChar, "CTYPE"))
	{
		return CTYPE;
	}
	else if (!strcmp(messageTagChar, "SURCH"))
	{
		return SURCH;
	}
	else if (!strcmp(messageTagChar, "CRDHA"))
	{
		return CRDHA;
	}
	else if (!strcmp(messageTagChar, "DISID"))
	{
		return DISID;
	}
	else if (!strcmp(messageTagChar, "DTEXT"))
	{
		return DTEXT;
	}
}

uint32_t ADVAM_RecordValueLength(void)
{
	uint8_t byte1, byte2, byte3;
	byte1 = ADVAM_CharHexToDec(ADVAM_ReadByteFromBuffer());
	byte2 = ADVAM_CharHexToDec(ADVAM_ReadByteFromBuffer());
	byte3 = ADVAM_CharHexToDec(ADVAM_ReadByteFromBuffer());
	
	return (byte1 * 0x100 + byte2 * 0x10 + byte3);
}

uint8_t ADVAM_ReadByteFromBuffer(void)
{
	/* Prevent function reading further than is avaliable */
	if (UARTEVSE_RxFifo.u8Rx1WriteRead == UARTEVSE_RxFifo.u8Rx1WriteIndex)
	{
		return 0x00;//UARTEVSE_RxFifo.USARTEVSE_RXBUFFER[UARTEVSE_RxFifo.u8Rx1WriteRead];
	}
	
	UARTEVSE_RxFifo.u8Rx1WriteRead = (UARTEVSE_RxFifo.u8Rx1WriteRead+1) % USARTEVSE_RXBUFFERSIZE;
	return UARTEVSE_RxFifo.USARTEVSE_RXBUFFER[UARTEVSE_RxFifo.u8Rx1WriteRead];
}

uint8_t ADVAM_NextByte(void)
{
	return UARTEVSE_RxFifo.USARTEVSE_RXBUFFER[(UARTEVSE_RxFifo.u8Rx1WriteRead + 1) % USARTEVSE_RXBUFFERSIZE];
}



uint8_t ADVAM_CharHexToDec(uint8_t character)
{
	if (character >= ASCII_ZERO && character <= ASCII_NINE)
	{
		/* The character is between 0 and 9 */
		return character - ASCII_ZERO;
	}
	else if (character >= ASCII_UPPER_CASE_A && character <= ASCII_UPPER_CASE_F)
	{
		/* The character is between A and F */
		return character - ASCII_UPPER_CASE_A + 10;
	}
	else if (character >= ASCII_LOWER_CASE_A && character <= ASCII_LOWER_CASE_F)
	{
		/* The character is between a and f */
		return character - ASCII_LOWER_CASE_A + 10;
	}
	else
	{
		return 0;
	}
}

uint8_t ADVAM_DectoCharHex(uint8_t number)
{
	if (number >= 0 && number <= 9)
	{
		/* The character is between 0 and 9 */
		return number + ASCII_ZERO;
	}
	else
	{
		/* The character is between 10 and 15 */
		return (number-10) + ASCII_A;
	}
}


void ADVAM_AddValueToRxPacket(ADVAM_MesTag_t messageTag, uint16_t length)
{
	switch (messageTag)
	{
	case MSGID:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.MSGID, length);
		break;
	case VERSN:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.VERSN, length);
		break;
	case TMOUT:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.TMOUT, length);
		break;
	case PRFIX:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.PRFIX, length);
		break;
	case ISPAY:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.ISPAY, length);
		break;
	case CRDTD:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.CRDTD, length);
		break;
	case STATS:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.STATS, length);
		break;
	case AMTTX:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.AMTTX, length);
		break;
	case EQUIP:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.EQUIP, length);
		break;
	case TXREF:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.TXREF, length);
		break;
	case RCODE:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.RCODE, length);
		break;
	case RTEXT:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.RTEXT, length);
		break;
	case TSTAN:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.TSTAN, length);
		break;
	case TAUTH:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.TAUTH, length);
		break;
	case TCARD:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.TCARD, length);
		break;
	case RECPT:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.RECPT, length);
		break;
	case SCHID:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.SCHID, length);
		break;
	case CTYPE:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.CTYPE, length);
		break;
	case SURCH:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.SURCH, length);
		break;
	case CRDHA:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.CRDHA, length);
		break;
	case DISID:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.DISID, length);
		break;
	case DTEXT:
		ADVAM_CopyFromBuffer(ADVAM_LastRxPacket.DTEXT, length);
		break;
	}
}

void ADVAM_CopyFromBuffer(char *dest, uint16_t length)
{
	uint16_t counter;
	for (counter = 0; counter < length; counter++)
	{
		dest[counter] = ADVAM_ReadByteFromBuffer();
	}
}

void ADVAM_SendACK(void)
{
	uint8_t buffer[3];
	buffer[0] = ADVAM_ACK_BYTE;
	buffer[1] = ADVAM_LastRxPacket.u8seqNum;
	
	// TO DO: Change to 232 or 485
	UARTEVSE_vTransmitMode();
	//HAL_Delay(10);
	HAL_UART_Transmit_IT(&HAL_UART_EVSE_Controller, buffer, 2);
}

void ADVAM_SendNAK(void)
{
	uint8_t buffer[3] = { ADVAM_NAK_BYTE, ADVAM_END_BYTE };
	
	// TO DO: Change to 232 or 485
	UARTEVSE_vTransmitMode();
	//HAL_Delay(10);
	HAL_UART_Transmit_IT(&HAL_UART_EVSE_Controller, buffer, 2);
}

uint8_t ADVAM_CheckRxCRC(void)
{
	uint16_t u16CRC_CalcBytes;
	int u16Length = ADVAM_GetCRCPacketLength();
	
	u16CRC_CalcBytes = ADVAM_GetCRC16(&UARTEVSE_RxFifo.USARTEVSE_RXBUFFER[ADVAM_LastRxPacket.u8Rx1StartByteBuffer + 1], u16Length, 0);
	

	if ((u16CRC_CalcBytes & 0XFF) == ADVAM_LastRxPacket.u8CRC[0] && (u16CRC_CalcBytes >> 8) == ADVAM_LastRxPacket.u8CRC[1])
	{
		return ADVAM_BAD_CRC;
	}
	else
	{
		return ADVAM_SUCCESSFUL;
	}
}

uint16_t ADVAM_GetCRCPacketLength(void)
{
	return (ADVAM_LastRxPacket.u8Rx1StopByteBuffer - ADVAM_LastRxPacket.u8Rx1StartByteBuffer + USARTEVSE_RXBUFFERSIZE) % USARTEVSE_RXBUFFERSIZE;
}

uint8_t ADVAM_ACKRecieved(void)
{
	// TO DO: ADD CHECK FOR SEQUENCE NUMBER
	if(ADVAM_LastRxPacket.CONTROL == ADVAM_ACK_BYTE)
	{
		ADVAM_SetRxPacketRead();
		return 1;
	} 
	else
	{
		return 0;
	}
}

uint8_t ADVAM_NAKRecieved(void)
{
	if (ADVAM_LastRxPacket.CONTROL == ADVAM_NAK_BYTE)
	{
		ADVAM_SetRxPacketRead();
		return 1;
	} 
	else
	{
		return 0;
	}
}

void ADVAM_SetRxPacketRead(void)
{
	ADVAM_ResetRxPacket();
	ADVAM_LastRxPacket.u8DataRead = ADVAM_Read;
}

void ADVAM_SetRxPacketUnread(void)
{
	ADVAM_LastRxPacket.u8DataRead = ADVAM_Unread;
}

uint8_t ADVAM_CheckPrevRxPacketRead(void)
{
	if (ADVAM_LastRxPacket.u8DataRead == ADVAM_Read)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void ADVAM_vWaitTimeUpdate(void)
{
	ADVAM_State.ADVAM_StateWaitForCard_Timeout++;
	ADVAM_State.ADVAM_StateLinkReq_Timeout++;
}
