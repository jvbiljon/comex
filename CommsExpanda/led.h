/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LED_H
#define __LED_H

#include <stm32f0xx_hal.h>
#include <stm32_hal_legacy.h>

#define NumberOfLEDs 3

#define GPIO_BLUE_RS232_1_LED GPIOA
#define GPIO_BLUE_RS232_2_LED GPIOB
#define GPIO_BLUE_RS485_1_LED GPIOB

#define GPIO_PIN_BLUE_RS232_1_LED GPIO_PIN_12
#define GPIO_PIN_BLUE_RS232_2_LED GPIO_PIN_0
#define GPIO_PIN_BLUE_RS485_1_LED GPIO_PIN_9

typedef enum
{
	on    = 0,
	off,
	flash
} LED_tenLEDModes;


typedef enum 
{
	blueRS232_1   = 0,
	blueRS232_2,
	blueRS485_1
} LED_tenLEDColour;

void LED_vRequestLED(LED_tenLEDColour u8LEDColour, LED_tenLEDModes u8LEDMode, uint16_t u16On, uint16_t u16Off);
void LED_vHandleLED(void);



#endif /* __LED_H */