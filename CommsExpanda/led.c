#include "led.h"
#include "systick.h"


static struct
{
	LED_tenLEDModes u8LEDMode;
	uint16_t u16LEDOnTime;
	uint16_t u16LEDOffTime;
	uint16_t u16LEDCount;
	// uint16_t u16LEDOffCount;
} LED__stLEDCurrentState[NumberOfLEDs];

void LED_vRequestLED(LED_tenLEDColour u8LEDColour, LED_tenLEDModes u8LEDMode, uint16_t u16On, uint16_t u16Off)
{
	LED__stLEDCurrentState[u8LEDColour].u16LEDOnTime = u16On;
	LED__stLEDCurrentState[u8LEDColour].u16LEDOffTime = u16Off;
	LED__stLEDCurrentState[u8LEDColour].u8LEDMode = u8LEDMode;
}

void LED_vHandleLED(void)
{
	switch (LED__stLEDCurrentState[blueRS232_1].u8LEDMode)
	{
	case on :
		HAL_GPIO_WritePin(GPIO_BLUE_RS232_1_LED, GPIO_PIN_BLUE_RS232_1_LED, GPIO_PIN_SET);
		break;
		
	case off :	
		HAL_GPIO_WritePin(GPIO_BLUE_RS232_1_LED, GPIO_PIN_BLUE_RS232_1_LED, GPIO_PIN_RESET);
		break;
	case flash :
		LED__stLEDCurrentState[blueRS232_1].u16LEDCount = (LED__stLEDCurrentState[blueRS232_1].u16LEDCount + 1) % (LED__stLEDCurrentState[blueRS232_1].u16LEDOffTime + LED__stLEDCurrentState[blueRS232_1].u16LEDOnTime);
		if (LED__stLEDCurrentState[blueRS232_1].u16LEDCount < LED__stLEDCurrentState[blueRS232_1].u16LEDOnTime)
			HAL_GPIO_WritePin(GPIO_BLUE_RS232_1_LED, GPIO_PIN_BLUE_RS232_1_LED, GPIO_PIN_SET);
		else
			HAL_GPIO_WritePin(GPIO_BLUE_RS232_1_LED, GPIO_PIN_BLUE_RS232_1_LED, GPIO_PIN_RESET);
		break;
	}
	
	switch (LED__stLEDCurrentState[blueRS232_2].u8LEDMode)
	{
	case on :
		HAL_GPIO_WritePin(GPIO_BLUE_RS232_2_LED, GPIO_PIN_BLUE_RS232_2_LED, GPIO_PIN_SET);
		break;
		
	case off :	
		HAL_GPIO_WritePin(GPIO_BLUE_RS232_2_LED, GPIO_PIN_BLUE_RS232_2_LED, GPIO_PIN_RESET);
		break;
	case flash :
		LED__stLEDCurrentState[blueRS232_2].u16LEDCount = (LED__stLEDCurrentState[blueRS232_2].u16LEDCount + 1) % (LED__stLEDCurrentState[blueRS232_2].u16LEDOffTime + LED__stLEDCurrentState[blueRS232_2].u16LEDOnTime);
		if (LED__stLEDCurrentState[blueRS232_2].u16LEDCount < LED__stLEDCurrentState[blueRS232_2].u16LEDOnTime)
			HAL_GPIO_WritePin(GPIO_BLUE_RS232_2_LED, GPIO_PIN_BLUE_RS232_2_LED, GPIO_PIN_SET);
		else
			HAL_GPIO_WritePin(GPIO_BLUE_RS232_2_LED, GPIO_PIN_BLUE_RS232_2_LED, GPIO_PIN_RESET);
		break;
	}
	
	switch (LED__stLEDCurrentState[blueRS485_1].u8LEDMode)
	{
	case on :
		HAL_GPIO_WritePin(GPIO_BLUE_RS485_1_LED, GPIO_PIN_BLUE_RS485_1_LED, GPIO_PIN_SET);
		break;
		
	case off :	
		LED__stLEDCurrentState[blueRS485_1].u16LEDCount = 0;
		HAL_GPIO_WritePin(GPIO_BLUE_RS485_1_LED, GPIO_PIN_BLUE_RS485_1_LED, GPIO_PIN_RESET);
		break;
	case flash :
		LED__stLEDCurrentState[blueRS485_1].u16LEDCount = (LED__stLEDCurrentState[blueRS485_1].u16LEDCount + 1) % (LED__stLEDCurrentState[blueRS485_1].u16LEDOffTime + LED__stLEDCurrentState[blueRS485_1].u16LEDOnTime);
		if (LED__stLEDCurrentState[blueRS485_1].u16LEDCount < LED__stLEDCurrentState[blueRS485_1].u16LEDOnTime)
			HAL_GPIO_WritePin(GPIO_BLUE_RS485_1_LED, GPIO_PIN_BLUE_RS485_1_LED, GPIO_PIN_SET);
		else
			/* Make single flash visible */
			LED__stLEDCurrentState[blueRS485_1].u8LEDMode = off;
			// HAL_GPIO_WritePin(GPIO_BLUE_RS485_1_LED, GPIO_PIN_BLUE_RS485_1_LED, GPIO_PIN_RESET);
		break;
	}
}