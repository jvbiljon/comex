/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STATE_H
#define __STATE_H

#include "main.h"

typedef enum
{
	STATE_nReset          = 0U,
	STATE_nInit,
	STATE_nRun,
	STATE_nShutdown,
	STATE_nSleep,
	STATE_nFailure,
	STATE_nNumberOfStates
} STATE_tenSystemStates;

/* call before main() */
void STATE_vInit(void);

/* call in main while(1) loop */
void STATE_vMain(void);


STATE_tenSystemStates STATE_enGetState(void);
void STATE_enSetState(STATE_tenSystemStates);

#endif /* __STATE_H */

