#ifndef __UART_H
#define __UART_H

#include "main.h"
#include "led.h"
#include "stm32f0xx_hal_uart.h"

#define USARTEVSE_RXBUFFERSIZE	500
#define USARTEVSE_RXPAYLOADSIZE	USARTEVSE_RXBUFFERSIZE-6
#define USARTEVSE_RXIDSIZE 3
#define UARTEVSE_RECIEVE_TIME 500//35

#define USARTPAYMENT1_RXBUFFERSIZE	1000
#define USARTPAYMENT1_TXPAYLOADSIZE	600

#define USARTPAYMENT2_RXBUFFERSIZE	1000
#define USARTPAYMENT2_TXPAYLOADSIZE	600

#define UART_PAYMENT1_RECIEVE_TIME 900
#define UART_PAYMENT2_RECIEVE_TIME 900

#define UART_END_BIT_LENGTH 2
#define UART_MINIMUM_LENGTH 5

#define UART_START_BYTE_RX 0x25
#define UART_STOP_BYTE_RX_1 0x25
#define UART_STOP_BYTE_RX_2 0x23
#define UART_START_BYTE_TX 0x40
#define UART_STOP_BYTE_TX_1 0x40
#define UART_STOP_BYTE_TX_2 0x23

#define PAYMENT_TERMINAL_ID_1_BYTE_0 0x30
#define PAYMENT_TERMINAL_ID_1_BYTE_1 0x31
#define PAYMENT_TERMINAL_ID_2_BYTE_0 0x30
#define PAYMENT_TERMINAL_ID_2_BYTE_1 0x32

#define PAYMENT_TERMINAL_ID_1 1
#define PAYMENT_TERMINAL_ID_2 2

#define USARTTEMPBUFFSIZE	2

#define GPIO_UARTEVSE_DIR GPIOA
#define GPIO_PIN_UARTEVSE_DIR GPIO_PIN_4

#define UART_REQ_REPLYWITHBUFFER 0x26
#define UART_REQ_SENDTOPAYMENT 0x2A
#define UART_REQ_RESETDEVICE 0x21
#define UART_REQ_NOMOREPACKETS 0x1B

#define UART_EVSE_NUMOFPACKETSTXBUFFER 7
#define UART_EMPTYBUFFER_MESLEN 7

#define ADVAM_START_BYTE 0x02
#define ADVAM_LAST_BYTE 0x03
#define ADVAM_ACK_BYTE 0x06
#define ADVAM_NAK_BYTE 0x15
#define ADVAM_WACK_BYTE 0x13

uint8_t USARTEVSE_TEMPBUFF[USARTTEMPBUFFSIZE];
uint8_t USARTPAYMENT1_TEMPBUFF[USARTTEMPBUFFSIZE];
uint8_t USARTPAYMENT2_TEMPBUFF[USARTTEMPBUFFSIZE];

typedef struct
{
	uint8_t USARTEVSE_RXBUFFER[USARTEVSE_RXBUFFERSIZE];
	uint16_t u8Rx1WriteIndex;
	uint16_t u8Rx1WriteRead;
	uint16_t u16WaitTime;
} UARTEVSE_tstRxFifo;

typedef struct
{
	uint8_t USARTPAYMENT1_RXBUFFER[USARTPAYMENT1_RXBUFFERSIZE];
	uint16_t u16Rx1WriteIndex;
	uint16_t u16Rx1WriteRead;
	uint16_t u16Rx1CheckIndex;
	uint16_t u16WaitTime;
} UARTPayment1_tstRxFifo;

typedef struct
{
	uint8_t USARTPAYMENT2_RXBUFFER[USARTPAYMENT2_RXBUFFERSIZE];
	uint16_t u16Rx2WriteIndex;
	uint16_t u16Rx2WriteRead;
	uint16_t u16Rx2CheckIndex;
	uint16_t u16WaitTime;
} UARTPayment2_tstRxFifo;

typedef struct
{
	uint8_t USARTEVSEID[USARTEVSE_RXIDSIZE];
	uint8_t USARTEVSEPAYLOAD[USARTEVSE_RXPAYLOADSIZE];
	uint8_t USARTEVSECRC;
	uint8_t USARTEVSECOMBYTE;
	uint16_t u8Rx1BufferLength;
	uint16_t u8Rx1StartByteBuffer;
	uint16_t u8Rx1StopByteBuffer;
} UARTEVSE_RxPacket_t;

typedef struct
{
	uint8_t USARTPAYMENT1PAYLOAD[USARTPAYMENT1_TXPAYLOADSIZE];
	uint16_t u16Tx1BufferLength;
} UARTPayment1_TxPacket_t;

typedef struct
{
	uint8_t USARTPAYMENT2PAYLOAD[USARTPAYMENT2_TXPAYLOADSIZE];
	uint16_t u16Tx2BufferLength;
} UARTPayment2_TxPacket_t;

typedef struct
{
	uint8_t UARTPAYLOAD[USARTPAYMENT1_TXPAYLOADSIZE];
	uint16_t u8BufferLength;
} UART_EVSETxPacket_Buffer_t;


typedef enum
{
	WaitResponse,
	CheckFirstByte,
	RecordIncomingData,
	ProcessData,
	TransmitFromBuffer,
	IncorrectDataRecieved
} UART_State_t;

typedef enum
{
	RecieveData,
	TransmitData
} UART_Mode_t;

void USART1_vClearTxBuffer(void);
void USART1_vClearRxBuffer(void);
void USART1_vClearRxIndex(void);

void UARTEVSEController_vState(void);
void UARTPaymentTerminal1_vState(void);
void UARTPaymentTerminal2_vState(void);

void UARTEVSE_vTransmitMode(void);
void UARTEVSE_vRecieveMode(void);

void UART_vWaitTimeUpdate(void);

uint8_t USARTEVSE_vCheckCRC(void);
uint8_t USARTPayment_vCalcCRC(uint8_t* u8MessageBuffer, uint16_t length);

void USART1_vSend(char data[], uint16_t u16size);
void USART1_vTransmitString(uint16_t u16size);

uint8_t UARTEVSE_Terminal1(uint8_t ID[2]);
uint8_t UARTEVSE_Terminal2(uint8_t ID[2]);

void UART_AddToEVSETxBuffer(uint8_t PAYLOAD[], uint16_t length, uint8_t u8ID);
void UART_SendFromEVSETxBuffer(uint8_t u8ID);
void UART_u8intcpy(uint8_t dest[], uint8_t loc[], uint16_t length);
void UART_EVSESendMessage(uint8_t *message, uint16_t length);
uint16_t UART_MessageLength(uint8_t *message);

uint8_t UARTPayment1_LastByte(void);
uint8_t UARTPayment2_LastByte(void);
uint8_t UARTPayment1_ACKByte(void);
uint8_t UARTPayment2_ACKByte(void);
uint8_t UARTPayment1_InsertedTxLastByte(void);
uint8_t UARTPayment2_InsertedTxLastByte(void);
uint8_t UARTPayment1_InsertedTxACKByte(void);
uint8_t UARTPayment2_InsertedTxACKByte(void);

uint8_t UART_CheckOverWriteEVSETxBuffer(uint8_t u8ID);

void UART_ResetBuffers(void);


#endif /* __UART_H */