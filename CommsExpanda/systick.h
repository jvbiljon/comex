/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SYSTICK_H
#define __SYSTICK_H

void SYSTCK_vHandleSystickFunctions(void);

#endif /* __SYSTICK_H */