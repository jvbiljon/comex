/**
  ******************************************************************************
  * @file    SysTick/stm32f0xx_it.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    23-March-2012
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "interrupts.h"
#include "main.h"
#include "systick.h"
#include "uart.h"

extern uint8_t USARTEVSE_TEMPBUFF[];
extern uint8_t USARTPAYMENT1_TEMPBUFF[];
extern uint8_t USARTPAYMENT2_TEMPBUFF[];

/** @addtogroup STM32F0_Discovery_Peripheral_Examples
  * @{
  */

/** @addtogroup SysTick_Example
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
//#define TXBUFFERSIZE   (countof(TxBuffer) - 1)
//#define RXBUFFERSIZE   0x20

/* Private macro -------------------------------------------------------------*/
//#define countof(a)   (sizeof(a) / sizeof(*(a)))

/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M0 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
	/* Go to infinite loop when Hard Fault exception occurs */
	while (1)
	{
	}
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
	HAL_IncTick();
	SYSTCK_vHandleSystickFunctions();
}

/******************************************************************************/
/*                 STM32F0xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f0xx.s).                                            */
/******************************************************************************/

/**
  * @brief  This function handles USART1 global interrupt request.
  * @param  None
  * @retval None
  */
void USART1_IRQHandler(void)
{
	/* Triggers callback function */
	HAL_UART_IRQHandler(&HAL_UART_Payment1);
	
	/* This resets the recieve flag so more data can be recieved */
	HAL_UART_Receive_IT(&HAL_UART_Payment1, (uint8_t *)USARTPAYMENT1_TEMPBUFF, 1);

}

void USART2_IRQHandler(void)
{
	/* Triggers callback function */
	HAL_UART_IRQHandler(&HAL_UART_EVSE_Controller);
	
	/* This resets the recieve flag so more data can be recieved */
	HAL_UART_Receive_IT(&HAL_UART_EVSE_Controller, (uint8_t *)USARTEVSE_TEMPBUFF, 1);

}

void USART3_4_IRQHandler(void)
{
	/* Triggers callback function */
	HAL_UART_IRQHandler(&HAL_UART_Payment2);
	
	/* This resets the recieve flag so more data can be recieved */
	HAL_UART_Receive_IT(&HAL_UART_Payment2, (uint8_t *)USARTPAYMENT2_TEMPBUFF, 1);

}

// TO DO: IMPLEMENT UART 1 AND 2


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/