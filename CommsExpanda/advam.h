#ifndef __ADVAM_H
#define __ADVAM_H

#include "uart.h"
#include "led.h"
#include <string.h>

#define ADVAM_START_BYTE 0x02
#define ADVAM_END_BYTE 0x03
#define ADVAM_ACK_BYTE 0x06
#define ADVAM_NAK_BYTE 0x15
#define ADVAM_WACK_BYTE 0x13

#define ADVAM_START_BYTE_POS 0
#define ADVAM_SEQNUM_INIT 0xFF
#define ADVAM_HEADER_SIZE 2

#define ADVAM_MESSAGETAGLENGTH 5

#define ADVAM_TXBUFFER_SIZE 250
#define ADVAM_TXPAYLOAD_SIZE ADVAM_TXBUFFER_SIZE-5
#define ADVAM_SUCCESSFUL 1
#define ADVAM_UNSUCCESSFUL 0
#define ADVAM_RESET 2
#define ADVAM_BAD_CRC 0

#define ADVAM_ENDOFSTRING 0x00//0x0A
#define ADVAM_FIRSTBYTE 0
#define ASCII_OFFSET 0x30
#define ASCII_ZERO 0x30
#define ASCII_NINE 0x39
#define ASCII_UPPER_CASE_A 0x41
#define ASCII_UPPER_CASE_F 0x46
#define ASCII_LOWER_CASE_A 0x61
#define ASCII_LOWER_CASE_F 0x66
#define ASCII_A 0x41

// TO DO: CALC THIS
#define ADVAM_BAUD_RATE 9600
#define ADVAM_PACKET_RECIEVE_TIME 900
#define ADVAM_ACK_RECIEVE_TIME 5

#define ADVAM_SURCHRESET 0x01
#define ADVAM_SURCHSET 0x00
#define ADVAM_CARDDET_TIMEOUT 60000
#define ADVAM_LINKREQ_TIMEOUT 1000
#define ADVAM_TKTREADREQ_ACK_TIMEOUT 1000


unsigned short ADVAM_CRCTab[256];

enum
{
	ADVAM_CRCDIN = 0x8005,
};

typedef uint8_t ADVAM_SeqNum_t;

typedef enum
{
	WaitForCard = 0,
	PreAuth,
	StartCharge,
	Charging,
	FinishCharge
} ADVAM_StateMain_t;

typedef enum
{
	LinkReq  = 0,
	DispPresentCard,
	CheckCardDet
} ADVAM_StateWaitForCard_t;

typedef enum
{
	SendLinkReq     = 0,
	CheckLinkReqACK,
	CheckLinkRes,
} ADVAM_StateLinkReq_t;

typedef enum
{
	SendCardDet,
	CheckCardDetACK,
	CheckCardDetRes
} ADVAM_StateCheckCardDet_t;

typedef enum
{
	SendTktReadReq     = 0,
	CheckTktReadReqACK,
	CheckTktReadRes,
} ADVAM_StateDispReq_t;

typedef enum
{
	ADVAM_WaitResponse     = 0,
	ADVAM_CheckFirstByte,
	ADVAM_RecordIncomingData,
	ADVAM_ProcessData,
	ADVAM_IncorrectDataRecieved
} ADVAM_StateUARTRx_t;


typedef struct
{
	ADVAM_StateMain_t ADVAM_StateMain;
	ADVAM_StateWaitForCard_t ADVAM_StateWaitForCard;
	uint32_t ADVAM_StateWaitForCard_Timeout;
	ADVAM_StateLinkReq_t ADVAM_StateLinkReq;
	uint32_t ADVAM_StateLinkReq_Timeout;
	ADVAM_StateUARTRx_t ADVAM_StateUART;
	ADVAM_StateUARTRx_t ADVAM_StateUART_ACK;
	ADVAM_StateCheckCardDet_t ADVAM_StateCheckCardDet;
} ADVAM_State_t;

typedef enum
{
	MSGID  = 0,
	VERSN,
	TMOUT,
	PRFIX,
	ISPAY,
	CRDTD,
	STATS,
	AMTTX,
	EQUIP,
	TXREF,
	RCODE,
	RTEXT,
	TSTAN,
	TAUTH,
	TCARD,
	RECPT,
	SCHID,
	CTYPE,
	SURCH,
	CRDHA,
	DISID,
	DTEXT
} ADVAM_MesTag_t;

typedef enum
{
	ADVAM_Read = 0,
	ADVAM_Unread
} ADVAM_Read_t;


typedef struct
{
	char MSGID[30];
	char VERSN[5];
	char TMOUT[3];
	char PRFIX[10];
	char ISPAY[2];
	char CRDTD[45];
	char STATS[2];
	char AMTTX[6];
	char EQUIP[12];
	char TXREF[24];
	char RCODE[3];
	char RTEXT[10];
	char TSTAN[10];
	char TAUTH[10];
	char TCARD[20];
	char RECPT[1024];
	char SCHID[2];
	char CTYPE[12];
	char SURCH[12];
	char CRDHA[60];
	char DISID[2];
	char DTEXT[30];
	char CONTROL;
	uint8_t u8seqNum;
	uint8_t u8CRC[3];
	uint16_t u8Rx1StartByteBuffer;
	uint16_t u8Rx1StopByteBuffer;
	ADVAM_Read_t u8DataRead;
} ADVAM_RxPacket_t;

typedef struct
{
	char MSGID[30];
	char VERSN[5];
	char TMOUT[3];
	char PRFIX[10];
	char ISPAY[2];
	char CRDTD[45];
	char STATS[2];
	char AMTTX[6];
	char EQUIP[12];
	char TXREF[24];
	char RCODE[3];
	char RTEXT[10];
	char TSTAN[10];
	char TAUTH[10];
	char TCARD[20];
	char SCHID[2];
	char CTYPE[12];
	char SURCH[12];
} ADVAM_TxPacket_t;

typedef struct
{
	char BUFFER[ADVAM_TXPAYLOAD_SIZE];
	uint16_t size;
} ADVAM_TxPayload_t;

typedef struct
{
	char PRFIX[10];
	char CRDTD[45];
	char CRDHA[30];
} ADVAM_CardData_t;

void ADVAM_vState(void);
void ADVAM_vSendLinkReq(void);
void ADVAM_vSendMessage(char data[], uint16_t u16size);
void ADVAM_CRCsetPoly(unsigned short poly);
uint16_t ADVAM_GetCRC16(const void *mem, int length, uint16_t crc);
uint8_t ADVAM_vCheckCard(void);
void ADVAM_SetMainState(ADVAM_StateMain_t State);
void ADVAM_SetWaitForCardState(ADVAM_StateWaitForCard_t State);
uint8_t ADVAM_vCheckLinkRes(void);
void ADVAM_vSendTxPacket(void);
void ADVAM_CombineStrings(char* combinedString, char* string1, char* string2, char* string3);
char* ADVAM_MessageLength(char* string);
void ADVAM_AddToTxPayload(char* tempString);
void ADVAM_ResetTxPacket(void);
void ADVAM_ResetRxPacket(void);
void ADVAM_UART_RxDataPacket(void);
void ADVAM_RecordSequenceNumber(void);
void ADVAM_RecordCRC(void);
uint8_t ADVAM_CheckRxCRC(void);
void ADVAM_NextPayload(void);
ADVAM_MesTag_t ADVAM_RecordMessageTag(void);
uint32_t ADVAM_RecordValueLength(void);
uint8_t ADVAM_ReadByteFromBuffer(void);
uint8_t ADVAM_NextByte(void);
void ADVAM_RecordPayload(void);
uint8_t ADVAM_CharHexToDec(uint8_t character);
void ADVAM_AddValueToRxPacket(ADVAM_MesTag_t messageTag, uint16_t length);
void ADVAM_CopyFromBuffer(char *dest, uint16_t length);
void ADVAM_SendACK(void);
void ADVAM_SendNAK(void);
uint16_t ADVAM_GetCRCPacketLength(void);
uint8_t ADVAM_ACKRecieved(void);
void ADVAM_SetRxPacketRead(void);
void ADVAM_SetRxPacketUnread(void);
uint8_t ADVAM_CheckPrevRxPacketRead(void);
uint8_t ADVAM_NAKRecieved(void);
void ADVAM_RecordControlByte(void);
void ADVAM_UART_RxACK(void);
void ADVAM_SetUARTRxACKState(ADVAM_StateUARTRx_t State);
void ADVAM_SendTktReadReq(void);
uint8_t ADVAM_LinkReq(void);
void ADVAM_SetLinkReqState(ADVAM_StateLinkReq_t State);
uint8_t ADVAM_DectoCharHex(uint8_t number);
uint8_t ADVAM_CheckCardDet(void);
void ADVAM_vWaitTimeUpdate(void);
void ADVAM_SetCheckCardDetState(ADVAM_StateCheckCardDet_t State);
uint8_t ADVAM_vCheckCardDetRes(void);
void ADVAM_RecordCardData(void);
void ADVAM_ClearDisplayRequests(void);

#endif /* __ADVAM_H */