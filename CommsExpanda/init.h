/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __INIT_H
#define __INIT_H

#include "main.h"
#include "led.h"
#include "uart.h"
#include "tim1.h"
#include "stm32f0xx_hal_uart.h"
#include "stm32f0xx_hal_conf.h"

void INIT_vCpu(void);
void INIT_vGpioConfig(void);
void INIT_vUsartEVSEController(void);
void INIT_vUsartPaymentTerminal1(void);
void INIT_vUsartPaymentTerminal2(void);
void INIT_vLED(void);
void SystemClock_Config(void);

void _Error_Handler(char *, int);

#endif /* __INIT_H */