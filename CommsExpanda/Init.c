#include "init.h"

extern uint8_t USARTEVSE_TEMPBUFF[];
extern uint8_t USARTPAYMENT1_TEMPBUFF[];
extern uint8_t USARTPAYMENT2_TEMPBUFF[];

void INIT_vCpu(void)
{
	/* Configure GPIO's */
	INIT_vGpioConfig();
	/* Configure USARTs */
	INIT_vUsartEVSEController();
	INIT_vUsartPaymentTerminal1();
	INIT_vUsartPaymentTerminal2();
}

/*
 * GPIO PORT	|	PIN		|	FUNCTION
 * -------------------------------------------
 * GPIOA		|	Pin 12	|	BLUE_RS232_1_LED
 * GPIOB		|	Pin 0	|	BLUE_RS232_2_LED
 * GPIOB		|	Pin 9	|	BLUE_RS485_1_LED
 * 
 **/

void INIT_vGpioConfig(void)
{
	/* Enable GPIO Clocks */
	__GPIOA_CLK_ENABLE();
	__GPIOB_CLK_ENABLE();

	/* Select GPIO Pins to set up */
	GPIO_InitStructure.Pin = GPIO_PIN_BLUE_RS232_1_LED;	
	
	/* Select Pin Setup Configuration */
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	GPIO_InitStructure.Pin = GPIO_PIN_BLUE_RS232_2_LED | GPIO_PIN_BLUE_RS485_1_LED;	
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);	

}



void INIT_vUsartEVSEController(void)
{
	__USART2_CLK_ENABLE();
	/* UART2 TX Pin */
	GPIO_InitStructure.Pin = GPIO_PIN_2;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Alternate = GPIO_AF1_USART2; 
	GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	/* UART2 RX Pin */
	GPIO_InitStructure.Pin = GPIO_PIN_3;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP; 
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	/* Direction Pin for RS-485 Connection */
	GPIO_InitStructure.Pin = GPIO_PIN_UARTEVSE_DIR;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIO_UARTEVSE_DIR, &GPIO_InitStructure);
	
	HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(USART2_IRQn);
	
	HAL_UART_EVSE_Controller.Instance          = USART2;
	HAL_UART_EVSE_Controller.Init.BaudRate     = 9600; //115200;  
	HAL_UART_EVSE_Controller.Init.WordLength   = UART_WORDLENGTH_8B;
	HAL_UART_EVSE_Controller.Init.StopBits     = UART_STOPBITS_1;
	HAL_UART_EVSE_Controller.Init.Parity       = UART_PARITY_NONE;
	HAL_UART_EVSE_Controller.Init.HwFlowCtl    = UART_HWCONTROL_NONE;
	HAL_UART_EVSE_Controller.Init.Mode         = UART_MODE_TX_RX;
	HAL_UART_EVSE_Controller.Init.HwFlowCtl    = UART_HWCONTROL_NONE;
	HAL_UART_EVSE_Controller.Init.OverSampling = UART_OVERSAMPLING_8;
	
	HAL_UART_Init(&HAL_UART_EVSE_Controller);
	
	UARTEVSE_vRecieveMode();
	HAL_UART_Receive_IT(&HAL_UART_EVSE_Controller, (uint8_t *)USARTEVSE_TEMPBUFF, 1);
}

void INIT_vUsartPaymentTerminal1(void)
{
	__USART1_CLK_ENABLE();
	/* UART1 TX Pin */
	GPIO_InitStructure.Pin = GPIO_PIN_9;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Alternate = GPIO_AF1_USART1;
	GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	/* UART1 RX Pin */
	GPIO_InitStructure.Pin = GPIO_PIN_10;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP; 
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(USART1_IRQn);
	
	HAL_UART_Payment1.Instance        = USART1;
	HAL_UART_Payment1.Init.BaudRate   = 9600;
	HAL_UART_Payment1.Init.WordLength = UART_WORDLENGTH_8B;
	HAL_UART_Payment1.Init.StopBits   = UART_STOPBITS_1;
	HAL_UART_Payment1.Init.Parity     = UART_PARITY_NONE;
	HAL_UART_Payment1.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	HAL_UART_Payment1.Init.Mode       = UART_MODE_TX_RX;
	
	HAL_UART_Init(&HAL_UART_Payment1);
	
	HAL_UART_Receive_IT(&HAL_UART_Payment1, (uint8_t *)USARTPAYMENT1_TEMPBUFF, 1);
}

void INIT_vUsartPaymentTerminal2(void)
{
	__USART3_CLK_ENABLE();
	/* UART3 TX Pin */
	GPIO_InitStructure.Pin = GPIO_PIN_10;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Alternate = GPIO_AF4_USART3;
	GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	/* UART1 RX Pin */
	GPIO_InitStructure.Pin = GPIO_PIN_11;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP; 
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	HAL_NVIC_SetPriority(USART3_4_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(USART3_4_IRQn);
	
	HAL_UART_Payment2.Instance        = USART3;
	HAL_UART_Payment2.Init.BaudRate   = 9600;
	HAL_UART_Payment2.Init.WordLength = UART_WORDLENGTH_8B;
	HAL_UART_Payment2.Init.StopBits   = UART_STOPBITS_1;
	HAL_UART_Payment2.Init.Parity     = UART_PARITY_NONE;
	HAL_UART_Payment2.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	HAL_UART_Payment2.Init.Mode       = UART_MODE_TX_RX;
	
	HAL_UART_Init(&HAL_UART_Payment2);
	
	HAL_UART_Receive_IT(&HAL_UART_Payment2, (uint8_t *)USARTPAYMENT2_TEMPBUFF, 1);
}

void INIT_vLED(void)
{
	/* Set LED States */
	LED_vRequestLED(blueRS232_1, off, 900, 300);
	LED_vRequestLED(blueRS232_2, off, 900, 300);
	LED_vRequestLED(blueRS485_1, off, 900, 300);
}


