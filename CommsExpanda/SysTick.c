#include "systick.h"
#include "led.h"
#include "uart.h"

/* Runs every 1mS */
void SYSTCK_vHandleSystickFunctions(void)
{
	LED_vHandleLED();
	UART_vWaitTimeUpdate();
}
