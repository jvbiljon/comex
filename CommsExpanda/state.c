#include "state.h"
#include "init.h"
#include "systick.h"
#include "interrupts.h"
#include "led.h"
#include "uart.h"
#include "tim1.h"

static STATE_tenSystemStates STATE__enCurrentState;

void STATE_vInit()
{
	STATE__enCurrentState = STATE_nReset;
}

void STATE_vMain(void)
{	
	switch (STATE__enCurrentState)
	{
	case STATE_nReset:
		{
			HAL_Init();
			STATE__enCurrentState = STATE_nInit;
		}
		break;

	case STATE_nInit:
		{
			INIT_vCpu();
			INIT_vLED();
			STATE__enCurrentState = STATE_nRun;	
		}	
		break;

	case STATE_nRun:
		{
			UARTEVSEController_vState();
			UARTPaymentTerminal1_vState();
			UARTPaymentTerminal2_vState();
		}
		break;

	case STATE_nShutdown:
		{
		}
		break;

	case STATE_nSleep:
		{
		}
		break;

	case STATE_nFailure:
		{
		}
		break;
            
	default:
		break;
	}
}

STATE_tenSystemStates STATE_enGetState(void)
{
	return STATE__enCurrentState;
}

void STATE_enSetState(STATE_tenSystemStates STATE__enNewState)
{
	STATE__enCurrentState = STATE__enNewState;
}